package com.jorgenunezvazquez.formulastats.mainapp.data.repositories

import com.jorgenunezvazquez.formulastats.domain.invoker.*
import com.jorgenunezvazquez.formulastats.domain.repositories.CountriesRepository
import instrumentation.repositories.CountriesRepositoryInstrumentation
import org.assertj.core.api.Assertions
import org.junit.Test

class CountriesRepositoryImplTest {

    private lateinit var countriesRepository: CountriesRepository

    @Test
    fun `given races repository, when getCountries returns a list of countries from local data source, then success result`() {
        countriesRepository = CountriesRepositoryInstrumentation.givenACountriesRepository()
        val result = countriesRepository.loadCountries()
        Assertions.assertThat(result is Success).isTrue()
        Assertions.assertThat((result as Success).data).isNotNull
    }

    @Test
    fun `given races repository, when getCountries throws Exception from local data source, then error result`() {
        countriesRepository = CountriesRepositoryInstrumentation.givenACountriesRepository(exception = Exception())
        val result = countriesRepository.loadCountries()
        Assertions.assertThat(result is Error).isTrue()
    }

    @Test
    fun `given races repository, when getCountries returns a list of countries from remote data source, then success result`() {
        countriesRepository = CountriesRepositoryInstrumentation.givenACountriesRepository(returnLocalEmptyList = true)
        val result = countriesRepository.loadCountries()
        Assertions.assertThat(result is Success).isTrue()
        Assertions.assertThat((result as Success).data).isNotNull
    }

    @Test
    fun `given races repository, when getCountries throws Exception from remote data source, then error result`() {
        countriesRepository = CountriesRepositoryInstrumentation.givenACountriesRepository(exception = Exception(), returnLocalEmptyList = true)
        val result = countriesRepository.loadCountries()
        Assertions.assertThat(result is Error).isTrue()
    }
}