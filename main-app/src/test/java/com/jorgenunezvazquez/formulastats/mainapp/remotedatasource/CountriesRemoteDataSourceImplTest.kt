package com.jorgenunezvazquez.formulastats.mainapp.remotedatasource

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries.CountriesRemoteDataSourceImpl
import com.jorgenunezvazquez.formulastats.remotedatasource.NetworkApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.CountriesApi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.module


class CountriesRemoteDataSourceImplTest : BaseRemoteDataSourceTest() {

    @Before
    override fun setUp() {
        super.setUp()
        startKoin {
            modules(module {
                single { NetworkApi().provideApi(getBaseUrl(), CountriesApi::class.java) }
            })
        }
    }

    @Test
    fun `given get countries then result is correct`() {
        enqueueFile("Countries.json")
        val result = CountriesRemoteDataSourceImpl().getCountries()
        assertThat(result).isNotEmpty
    }

    @Test
    fun `given get countries when error then result is empty list`() {
        enqueueFile("Countries.json", responseCode = 500)
        val result = CountriesRemoteDataSourceImpl().getCountries()
        assertThat(result).isEmpty()
    }
}