package com.jorgenunezvazquez.formulastats.mainapp.remotedatasource

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia.ImagesRemoteDataSourceImpl
import com.jorgenunezvazquez.formulastats.remotedatasource.NetworkApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.WikiApi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.module


class ImagesRemoteDataSourceImplTest : BaseRemoteDataSourceTest() {

    @Before
    override fun setUp() {
        super.setUp()
        startKoin {
            modules(module {
                single { NetworkApi().provideApi(getBaseUrl(), WikiApi::class.java) }
            })
        }
    }

    @Test
    fun `given get image url from wiki then result is correct`() {
        enqueueFile("WikiImages.json")
        val result = ImagesRemoteDataSourceImpl().getImageFromWiki(pageTitle = "alonso")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given get image url from wiki then result is exception`() {
        enqueueFile("WikiImages.json", responseCode = 500)
        ImagesRemoteDataSourceImpl().getImageFromWiki(pageTitle = "alonso")
    }
}