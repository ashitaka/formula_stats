package com.jorgenunezvazquez.formulastats.mainapp.data.repositories

import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import instrumentation.remote.RemoteDataSourceStatus
import instrumentation.remote.StatsRemoteDataSourceConfig
import instrumentation.repositories.StatsRepositoryInstrumentation.givenAStatsRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class StatsRepositoryImplTest {

    private lateinit var statsRepository: StatsRepository

    @Test
    fun `given drivers repository, when getDriverSeasonStandings returns list of driver season standings, then success result`() {
        statsRepository = givenAStatsRepository(statsRemoteDataSourceConfig = StatsRemoteDataSourceConfig(statsRemoteDataSourceStatus = RemoteDataSourceStatus.SUCCESS))
        val result = statsRepository.getDriverSeasonStandings("2019")
        assertThat(result is Success).isTrue()
        assertThat((result as Success).data).isNotNull()
    }

    @Test
    fun `given drivers repository, when getDriverSeasonStandings returns an Exception, then error result`() {
        statsRepository = givenAStatsRepository(statsRemoteDataSourceConfig = StatsRemoteDataSourceConfig(statsRemoteDataSourceStatus = RemoteDataSourceStatus.FAILURE))
        val result = statsRepository.getDriverSeasonStandings("2019")
        assertThat(result is Error).isTrue()
    }

    @Test
    fun `given drivers repository, when getDriverRaceResults returns a driver result, then success result`() {
        statsRepository = givenAStatsRepository(statsRemoteDataSourceConfig = StatsRemoteDataSourceConfig(statsRemoteDataSourceStatus = RemoteDataSourceStatus.SUCCESS))
        val result = statsRepository.getDriverRaceResults("driverId")
        assertThat(result is Success).isTrue()
        assertThat((result as Success).data.results).isNotEmpty
    }

    @Test
    fun `given drivers repository, when getDriverRaceResults throws an Exception, then error result`() {
        statsRepository = givenAStatsRepository(statsRemoteDataSourceConfig = StatsRemoteDataSourceConfig(statsRemoteDataSourceStatus = RemoteDataSourceStatus.FAILURE))
        val result = statsRepository.getDriverRaceResults("driverId")
        assertThat(result is Error).isTrue()
    }
}