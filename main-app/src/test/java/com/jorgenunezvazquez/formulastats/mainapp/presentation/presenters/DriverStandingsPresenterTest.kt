package com.jorgenunezvazquez.formulastats.mainapp.presentation.presenters

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriversCurrentSeasonStandingsUseCase
import com.jorgenunezvazquez.formulastats.mainapp.MyRobolectricTestRunner
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverStandingsPresenter
import instrumentation.DriverStandingsViewMethod
import instrumentation.DriversCallbackResult
import instrumentation.StatsInstrumentation
import instrumentation.StatsInstrumentation.givenStatsRepository
import instrumentation.repositories.ImagesRepositoryInstrumentation.givenAImagesRepository
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module

class DriverStandingsPresenterTest : MyRobolectricTestRunner() {

    private lateinit var driverStandingsPresenter: DriverStandingsPresenter
    private lateinit var driversCallbackResult: DriversCallbackResult
    private lateinit var getDriverSeasonStandingsResult: Result<DriverStandingsWrapper>
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
        startKoin {
            modules(module { androidContext(context) })
        }
        driversCallbackResult = DriversCallbackResult()
        driverStandingsPresenter = givenADriversPresenter()
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test
    fun `when getDrivers returns a list, show drivers is called`() {
        getDriverSeasonStandingsResult = Success(StatsInstrumentation.givenADriverStandingsWrapper())
        driverStandingsPresenter.onViewCreated()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.SHOW_LOADING)).isTrue()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.LOAD_DRIVERS)).isTrue()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.HIDE_LOADING)).isTrue()
    }

    @Test
    fun `when getDrivers returns an error, show error is called`() {
        getDriverSeasonStandingsResult = Error()
        driverStandingsPresenter.onViewCreated()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.SHOW_LOADING)).isTrue()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.SHOW_ERROR)).isTrue()
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.HIDE_LOADING)).isTrue()
    }

    @Test
    fun `when driver is clicked, we open driver details screen`() {
        getDriverSeasonStandingsResult = Success(StatsInstrumentation.givenADriverStandingsWrapper())
        driverStandingsPresenter.onViewCreated()
        driverStandingsPresenter.onDriverClicked("jorge")
        Assertions.assertThat(driversCallbackResult.isMethodFired(DriverStandingsViewMethod.GO_TO_DRIVER_DETAILS)).isTrue()
    }

    private fun givenADriversPresenter(): DriverStandingsPresenter =
        DriverStandingsPresenter(
            StatsInstrumentation.givenADriversViewContract(driversCallbackResult),
            givenAGetDriversSeasonStandingsUseCase(), StatsInstrumentation.givenAnInvoker()
        )

    private fun givenAGetDriversSeasonStandingsUseCase(): GetDriversCurrentSeasonStandingsUseCase {
        return GetDriversCurrentSeasonStandingsUseCase(
            givenStatsRepository(getDriverSeasonStandingsResult = { getDriverSeasonStandingsResult }),
            givenAImagesRepository()
        )
    }
}