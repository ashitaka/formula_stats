package com.jorgenunezvazquez.formulastats.mainapp.domain

import com.jorgenunezvazquez.formulastats.domain.invoker.*
import instrumentation.usecases.GetAllDriverResultByIdUseCaseInstruments
import org.assertj.core.api.Assertions
import org.junit.Test

class GetAllDriversResultByIdUseCaseTest {

    @Test
    fun `given a GetAllDriverResultById use case, then invoke with success result`() {
        val useCase = GetAllDriverResultByIdUseCaseInstruments.givenASuccessResult()
        val result = useCase.run()
        Assertions.assertThat(result).isNotNull()
        Assertions.assertThat(result is Success).isTrue()
    }

    @Test
    fun `given a GetAllDriverResultById use case, then invoke with unknown error result`() {
        val useCase = GetAllDriverResultByIdUseCaseInstruments.givenFailureResult()
        val result = useCase.run()
        Assertions.assertThat(result).isNotNull()
        Assertions.assertThat(result is Error).isTrue()
    }
}