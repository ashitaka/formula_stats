package com.jorgenunezvazquez.formulastats.mainapp.remotedatasource

import instrumentation.domain.StatsInstrumentation.givenARace
import com.jorgenunezvazquez.formulastats.remotedatasource.NetworkApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.ErgastApi
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.StatsRemoteDataSourceImpl
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.module


class StatsRemoteDataSourceImplTest : BaseRemoteDataSourceTest() {

    @Before
    override fun setUp() {
        super.setUp()
        startKoin {
            modules(module {
                single { NetworkApi().provideApi(getBaseUrl(), ErgastApi::class.java) }
            })
        }
    }

    @Test
    fun `given race schedule request then result is correct`() {
        enqueueFile("RaceSchedule.json")
        val result = StatsRemoteDataSourceImpl().getCurrentRaceSchedule()
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given race schedule request then result is exception`() {
        enqueueFile("RaceSchedule.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getCurrentRaceSchedule()
    }

    @Test
    fun `given drivers standings request then result is correct`() {
        enqueueFile("DriversSeasonStandings.json")
        val result = StatsRemoteDataSourceImpl().getDriversSeasonStandings("current")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given drivers standings request then result is exception`() {
        enqueueFile("DriversSeasonStandings.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getDriversSeasonStandings("current")
    }

    @Test
    fun `given constructor standings request then result is correct`() {
        enqueueFile("ConstructorsSeasonStandings.json")
        val result = StatsRemoteDataSourceImpl().getConstructorSeasonStandings()
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given constructor standings request then result is exception`() {
        enqueueFile("ConstructorsSeasonStandings.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getConstructorSeasonStandings()
    }

    @Test
    fun `given driver results by race request then result is correct`() {
        enqueueFile("DriversRaceResults.json")
        val result = StatsRemoteDataSourceImpl().getDriverResultsByRace(givenARace())
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given driver results by race request then result is exception`() {
        enqueueFile("DriversRaceResults.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getDriverResultsByRace(givenARace())
    }

    @Test
    fun `given driver standings by season then result is correct`() {
        enqueueFile("DriversSeasonStandings.json")
        val result = StatsRemoteDataSourceImpl().getDriverStandingsBySeason("current", "hamilton")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given driver standings by season then result is exception`() {
        enqueueFile("DriversSeasonStandings.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getDriverStandingsBySeason("current", "hamilton")
    }

    @Test
    fun `given get all drivers race results then result is correct`() {
        enqueueFile("DriverAllResults.json")
        val result = StatsRemoteDataSourceImpl().getAllDriverRaceResults("mick_schumacher")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given get all drivers race results then result is exception`() {
        enqueueFile("DriverAllResults.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getAllDriverRaceResults("mick_schumacher")
    }

    @Test
    fun `given get driver championships then result is correct`() {
        enqueueFile("DriverChampionships.json")
        val result = StatsRemoteDataSourceImpl().getDriverChampionships("alonso")
        assertThat(result).isEqualTo(2)
    }

    @Test(expected = Exception::class)
    fun `given get driver championships then result is exception`() {
        enqueueFile("DriverChampionships.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getDriverChampionships("alonso")
    }

    @Test
    fun `given get driver results by season then result is correct`() {
        enqueueFile("DriverSeasonResults.json")
        val result = StatsRemoteDataSourceImpl().getDriverResultsBySeason("2022","alonso")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given get driver results by season then result is exception`() {
        enqueueFile("DriverSeasonResults.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getDriverResultsBySeason("2022","alonso")
    }

    @Test
    fun `given get all constructor standings by id then result is correct`() {
        enqueueFile("ConstructorAllStandings.json")
        val result = StatsRemoteDataSourceImpl().getAllConstructorStandingsById("mclaren")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given get all constructor standings by id then result is exception`() {
        enqueueFile("ConstructorAllStandings.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getAllConstructorStandingsById("mclaren")
    }

    @Test
    fun `given get all constructor standings by season then result is correct`() {
        enqueueFile("ConstructorSeasonStandings.json")
        val result = StatsRemoteDataSourceImpl().getConstructorStandingsBySeason("2022","mclaren")
        assertThat(result).isNotNull
    }

    @Test(expected = Exception::class)
    fun `given get all constructor standings by season then result is exception`() {
        enqueueFile("ConstructorSeasonStandings.json", responseCode = 500)
        StatsRemoteDataSourceImpl().getConstructorStandingsBySeason("2022","mclaren")
    }
}