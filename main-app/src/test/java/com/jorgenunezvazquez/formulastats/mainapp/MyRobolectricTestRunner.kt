package com.jorgenunezvazquez.formulastats.mainapp

import android.app.Application
import android.os.Build
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(application = MockApplication::class, sdk = [Build.VERSION_CODES.O_MR1])
abstract class MyRobolectricTestRunner

class MockApplication : Application()
