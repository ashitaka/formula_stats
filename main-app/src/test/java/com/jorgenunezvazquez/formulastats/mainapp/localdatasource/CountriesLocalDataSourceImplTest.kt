package com.jorgenunezvazquez.formulastats.mainapp.localdatasource

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.localdatasource.CountriesLocalDataSourceImpl
import com.jorgenunezvazquez.formulastats.localdatasource.db.AppDatabase
import com.jorgenunezvazquez.formulastats.mainapp.MyRobolectricTestRunner
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module

class CountriesLocalDataSourceImplTest : MyRobolectricTestRunner() {

    private lateinit var appDatabase: AppDatabase

    @Before
    fun before() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).allowMainThreadQueries().build()
        startKoin {
            androidContext(context)
            modules(module { single { appDatabase } })
        }
    }

    @After
    fun after() {
        appDatabase.close()
        stopKoin()
    }

    @Test
    fun `given countries database then countries can be saved and retrieved`() {
        val countries = getCountryList()
        CountriesLocalDataSourceImpl().saveCountries(countries)
        val savedCountries = CountriesLocalDataSourceImpl().getCountries()
        assertThat(savedCountries).isEqualTo(countries)
    }

    @Test
    fun `given countries database when get flag url by country then is success`() {
        val countries = getCountryList()
        CountriesLocalDataSourceImpl().saveCountries(countries)
        val countryFlag = CountriesLocalDataSourceImpl().getCountryFlagUrlByCountry("Germany")
        assertThat(countryFlag).isEqualTo("https://countryflagsapi.com/png/DE")
    }

    @Test
    fun `given countries database when get flag url by nationality then is success`() {
        val countries = getCountryList()
        CountriesLocalDataSourceImpl().saveCountries(countries)
        val countryFlag = CountriesLocalDataSourceImpl().getCountryFlagUrlByNationalityString("Italian")
        assertThat(countryFlag).isEqualTo("https://countryflagsapi.com/png/IT")
    }

    private fun getCountryList() = listOf(
        CountryData("Spain", "ES", "Spanish"),
        CountryData("Italy", "IT", "Italian"),
        CountryData("Germany", "DE", "German")
    )
}
