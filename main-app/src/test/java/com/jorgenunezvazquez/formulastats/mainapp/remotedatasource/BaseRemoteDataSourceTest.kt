package com.jorgenunezvazquez.formulastats.mainapp.remotedatasource

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.koin.core.context.stopKoin


open class BaseRemoteDataSourceTest {

    lateinit var mockServer: MockWebServer

    @Before
    open fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()
    }

    @After
    open fun tearDown() {
        mockServer.shutdown()
        stopKoin()
    }

    fun getBaseUrl() = mockServer.url("/").toString()

    fun enqueueFile(fileName: String, responseCode: Int = 200){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(fileName)
        val body = inputStream.source().buffer().readUtf8()
        mockServer.enqueue(MockResponse().setResponseCode(responseCode).setBody(body))
    }
}