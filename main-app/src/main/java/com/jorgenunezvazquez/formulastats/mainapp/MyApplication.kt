package com.jorgenunezvazquez.formulastats.mainapp

import android.app.Application
import com.jorgenunezvazquez.formulastats.data.injection.DataModules
import com.jorgenunezvazquez.formulastats.domain.injection.DomainModules
import com.jorgenunezvazquez.formulastats.localdatasource.injection.LocalModules
import com.jorgenunezvazquez.formulastats.presentation.injection.PresentationModules
import com.jorgenunezvazquez.formulastats.remotedatasource.injection.RemoteModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MyApplication)
            modules(
                listOf(
                    DataModules.appModule,
                    DomainModules.domainModule,
                    PresentationModules.presentationModule,
                    RemoteModules.remoteModules,
                    LocalModules.localModules
                )
            )
            EmptyLogger()
        }
    }
}