package com.jorgenunezvazquez.formulastats.mainapp

import androidx.test.core.app.ActivityScenario
import androidx.test.runner.AndroidJUnitRunner
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.adevinta.android.barista.interaction.BaristaClickInteractions.clickOn
import com.jorgenunezvazquez.formulastats.R
import com.jorgenunezvazquez.formulastats.mainapp.MainActivityRobot.mainScreen
import instrumentation.remote.StatsRemoteDataSourceConfig
import instrumentation.remote.StatsRemoteDataSourceInstruments
import com.jorgenunezvazquez.formulastats.presentation.main.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.stopKoin
import org.koin.dsl.module

class MainActivityTest : AndroidJUnitRunner() {

    private lateinit var activityScenarioRule: ActivityScenario<MainActivity>
    private var statsRemoteDataSourceConfig: StatsRemoteDataSourceConfig = StatsRemoteDataSourceConfig()

    @Before
    fun setUp() {
        val mockModules = module {
            factory { StatsRemoteDataSourceInstruments.givenStatsRemoteDataSource(statsRemoteDataSourceConfig) }
        }
        loadKoinModules(mockModules)
        activityScenarioRule = ActivityScenario.launch(MainActivity::class.java)
    }

    @After
    fun close() {
        activityScenarioRule.close()
        stopKoin()
    }

    @Test
    fun on_activity_started_has_correct_content() {
        mainScreen {
            hasCorrectCommonContent()
        }
    }

    @Test
    fun on_drivers_clicked_shows_drivers() {
        mainScreen {
            goToDriversFragment()
            listIsShown()
            clickOnFirstItem()
            driverDetailIsShown()
        }
    }

    @Test
    fun on_constructors_clicked_shows_constructors() {
        mainScreen {
            goToConstructorsFragment()
            listIsShown()
        }
    }

    @Test
    fun on_races_clicked_shows_races() {
        clickOn(R.id.racesFragment)
        assertDisplayed(R.id.homeRecyclerView)
    }
}