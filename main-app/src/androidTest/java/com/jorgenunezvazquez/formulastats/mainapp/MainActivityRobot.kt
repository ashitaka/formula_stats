package com.jorgenunezvazquez.formulastats.mainapp

import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.adevinta.android.barista.interaction.BaristaClickInteractions.clickOn
import com.adevinta.android.barista.interaction.BaristaListInteractions.clickListItem
import com.jorgenunezvazquez.formulastats.R

object MainActivityRobot {

    fun mainScreen(func: MainActivityRobot.() -> Unit) = MainActivityRobot.apply(func)

    fun hasCorrectCommonContent() {
        assertDisplayed(R.id.bottomNavigationView)
        assertDisplayed(R.id.nav_host_fragment)
        assertDisplayed(R.id.myToolbar)
        assertDisplayed(R.id.homeRecyclerView)
    }

    fun goToDriversFragment() {
        clickOn(R.id.driversFragment)
    }

    fun goToConstructorsFragment() {
        clickOn(R.id.constructorsFragment)
    }

    fun listIsShown() {
        assertDisplayed(R.id.homeRecyclerView)
    }

    fun clickOnFirstItem() {
        clickListItem(R.id.homeRecyclerView, position = 0)
    }

    fun driverDetailIsShown() {
        assertDisplayed(R.id.driverDetailContainer)
    }
}