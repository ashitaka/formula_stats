package instrumentation.domain

import com.jorgenunezvazquez.formulastats.domain.model.constructors.Constructor
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.domain.model.drivers.*
import com.jorgenunezvazquez.formulastats.domain.model.races.Race

object StatsInstrumentation {

    fun givenADriverTotalResult() = DriverTotalResult(
        "0",
        mutableListOf(givenADriverRaceResult())
    )

    fun givenACountryLocalData() = CountryData("GZ", "Galiza", "galego")

    fun givenADriverRaceResult() = DriverRaceResult(
        "2019",
        "21",
        "www.jnv.com",
        "raceName",
        Circuit("", "", "", Location()),
        "",
        results = givenADriverResult()
    )

    fun givenADriverResult() = DriverResult(
        "",
        "1",
        "25",
        givenADriver(),
        givenAConstructor(),
        "18",
        "60",
        "",
        Time("", ""),
        FastestLap("", "", FastestLapTime(""), AverageSpeed("", ""))
    )

    fun givenADriverStandingsWrapper() = DriverStandingsWrapper(
        "2019",
        "19",
        listOf(givenDriverStandings())
    )

    fun givenDriverStandings() = DriverStandings(
        position = "1",
        positionText = "1",
        points = "50",
        wins = "1",
        driver = givenADriver(),
        constructors = listOf(givenAConstructor())
    )

    fun givenADriver() = Driver(
        driverId = "alonso",
        permanentNumber = "14",
        code = "ALO",
        url = "http:\\/\\/en.wikipedia.org\\/wiki\\/Fernando_Alonso",
        givenName = "Fernando",
        familyName = "Alonso",
        dateOfBirth = "1981-07-29",
        nationality = "Spanish"
    )

    fun givenAConstructor() = Constructor(
        constructorId = "alpine",
        url = "http://en.wikipedia.org/wiki/Alpine_F1_Team",
        name = "Alpine F1 Team",
        nationality = "French"
    )

    fun givenAConstructorsStandingsWrapper() = ConstructorStandingsWrapper(
        "2019",
        "19",
        mutableListOf()
    )

    fun givenARace() = Race(
        season = "2018",
        round = "22",
        url = "race url",
        raceName = "race name",
        Circuit(
            circuitId = "circuit Id",
            url = "circuit url",
            circuitName = "circuit name",
            location = Location(lat = "42.4242", long = "8.3333", locality = "O Carballo", country = "GZ")
        ),
        date = "",
        time = "",
        driverResults = emptyList(),
        imageUrl = null,
        flagUrl = null
    )
}