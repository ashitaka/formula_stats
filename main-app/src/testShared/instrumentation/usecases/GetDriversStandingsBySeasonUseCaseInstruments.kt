package instrumentation.usecases

import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriverStandingsBySeasonUseCase
import instrumentation.DriversRepositoryInstruments
import instrumentation.RepositoryStatus

object GetDriversStandingsBySeasonUseCaseInstruments {
    fun givenASuccessResult() = GetDriverStandingsBySeasonUseCase(
            DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.SUCCESS)
    )

    fun givenFailureResult() = GetDriverStandingsBySeasonUseCase(
            DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.FAILURE)
    )
}