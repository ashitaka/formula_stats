package instrumentation.usecases

import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriversCurrentSeasonStandingsUseCase
import instrumentation.DriversRepositoryInstruments
import instrumentation.RepositoryStatus
import instrumentation.repositories.ImagesRepositoryInstrumentation.givenAImagesRepository

object GetDriversSeasonStandingsUseCaseInstruments {
    fun givenASuccessResult() = GetDriversCurrentSeasonStandingsUseCase(
        DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.SUCCESS),
        givenAImagesRepository()
    )

    fun givenFailureResult() = GetDriversCurrentSeasonStandingsUseCase(
        DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.FAILURE),
        givenAImagesRepository()
    )
}