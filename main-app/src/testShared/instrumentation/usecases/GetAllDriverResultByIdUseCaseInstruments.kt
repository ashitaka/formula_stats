package instrumentation.usecases

import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetAllDriverResultsByIdUseCase
import instrumentation.DriversRepositoryInstruments
import instrumentation.RepositoryStatus

object GetAllDriverResultByIdUseCaseInstruments {
    fun givenASuccessResult() = GetAllDriverResultsByIdUseCase(
        DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.SUCCESS)
    ) withParams "hamilton"

    fun givenFailureResult() = GetAllDriverResultsByIdUseCase(
        DriversRepositoryInstruments.givenDriversRepositoryInstruments(RepositoryStatus.FAILURE)
    ) withParams "hamilton"
}