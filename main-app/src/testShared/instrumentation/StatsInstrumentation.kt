package instrumentation

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.Callback
import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.*
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverStandingsModel
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriversViewContract
import instrumentation.domain.StatsInstrumentation.givenAConstructorsStandingsWrapper
import instrumentation.domain.StatsInstrumentation.givenADriverRaceResult
import instrumentation.domain.StatsInstrumentation.givenADriverResult
import instrumentation.domain.StatsInstrumentation.givenADriverTotalResult
import instrumentation.domain.StatsInstrumentation.givenARace

enum class DriverStandingsViewMethod {
    LOAD_DRIVERS,
    SHOW_ERROR,
    SHOW_LOADING,
    HIDE_LOADING,
    GO_TO_DRIVER_DETAILS,
    HIDE_ERROR
}

class DriversCallbackResult : BaseCallbackResult<DriverStandingsViewMethod>()

object StatsInstrumentation {

    fun givenADriversViewContract(driversCallbackResult: DriversCallbackResult): DriversViewContract {

        return object : DriversViewContract {

            override fun showDriversList(driverStandingsModel: List<DriverStandingsModel>) {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.LOAD_DRIVERS)
            }

            override fun showError() {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.SHOW_ERROR)
            }

            override fun showLoader() {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.SHOW_LOADING)
            }

            override fun hideLoader() {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.HIDE_LOADING)
            }

            override fun goToDriverDetails(driverStandings: DriverStandings, round: String, season: String) {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.GO_TO_DRIVER_DETAILS)
            }

            override fun hideError() {
                driversCallbackResult.putMethodCall(DriverStandingsViewMethod.HIDE_ERROR)
            }
        }
    }

    fun givenStatsRepository(getDriverSeasonStandingsResult: (() -> Result<DriverStandingsWrapper>)): StatsRepository {

        return object : StatsRepository {
            override fun getDriverRaceResults(driverId: String): Result<DriverTotalResult> = Success(givenADriverTotalResult())

            override fun getCurrentRaceSchedule(): Result<List<Race>> = Success(listOf(givenARace()))

            override fun getDriverResultsByRace(race: Race): Result<List<DriverResult>> = Success(listOf(givenADriverResult()))

            override fun getDriverStandingsBySeason(season: String, driverId: String): Result<DriverStandingsWrapper> = Success(givenADriverStandingsWrapper())

            override fun getDriverSeasonStandings(season: String): Result<DriverStandingsWrapper> = getDriverSeasonStandingsResult.invoke()

            override fun getDriverResultsBySeason(season: String, driverId: String): Result<List<DriverRaceResult>> = Success(listOf(givenADriverRaceResult()))

            override fun getConstructorSeasonStandings(): Result<ConstructorStandingsWrapper> = Success(givenAConstructorsStandingsWrapper())

            override fun getConstructorStandingsBySeason(season: String, constructorId: String): Result<ConstructorStandingsWrapper> = Success(givenAConstructorsStandingsWrapper())

            override fun getAllConstructorStandingsById(constructorId: String): Result<List<ConstructorStandingsWrapper>> = Success(listOf(givenAConstructorsStandingsWrapper()))
        }
    }

    fun givenAnInvoker(): Invoker {
        return object : Invoker {
            override fun <P, R> execute(useCase: BaseUseCase<P, R>, callback: Callback<R>?) {
                callback?.invoke(useCase.run())
            }

            override fun cancelTasks() = Unit
        }
    }

    fun givenADriverStandingsWrapper() = DriverStandingsWrapper(
        "2019",
        "19",
        mutableListOf(givenDriverStandings())
    )

    private val driverDomain = Driver(
        "jorge",
        "13",
        "JOR",
        "www.aaa.com",
        "jorge",
        "nunez",
        "13/09/86",
        "spain",
        "",
        ""
    )

    private fun givenDriverStandings() = DriverStandings(
        "1",
        "1",
        "100",
        "10",
        driverDomain,
        emptyList()
    )
}