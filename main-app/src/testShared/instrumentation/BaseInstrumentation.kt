package instrumentation

abstract class BaseCallbackResult<T> {

    var firedMethods = mutableListOf<FiredMethod<T>>()

    fun isMethodFired(methodName: T): Boolean {
        return firedMethods.contains(FiredMethod(methodName))
    }

    fun putMethodCall(methodName: T) {
        firedMethods.add(FiredMethod(methodName))
    }
}

data class FiredMethod<T>(
    val method: T
)