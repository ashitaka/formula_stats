package instrumentation

import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverTotalResult
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import instrumentation.domain.StatsInstrumentation.givenADriverStandingsWrapper
import instrumentation.domain.StatsInstrumentation.givenADriverTotalResult


enum class RepositoryStatus {
    SUCCESS, FAILURE
}

object DriversRepositoryInstruments {

    fun givenDriversRepositoryInstruments(repositoryStatus: RepositoryStatus): StatsRepository {
        return object : StatsRepository {

            override fun getDriverRaceResults(driverId: String): Result<DriverTotalResult> {
                return when (repositoryStatus) {
                    RepositoryStatus.SUCCESS -> Success(givenADriverTotalResult())
                    RepositoryStatus.FAILURE -> Error()
                }
            }

            override fun getDriverSeasonStandings(season: String): Result<DriverStandingsWrapper> {
                return when (repositoryStatus) {
                    RepositoryStatus.SUCCESS -> Success(givenADriverStandingsWrapper())
                    RepositoryStatus.FAILURE -> Error()
                }
            }

            override fun getDriverResultsBySeason(season: String, driverId: String): Result<List<DriverRaceResult>> {
                TODO("Not yet implemented")
            }

            override fun getConstructorSeasonStandings(): Result<ConstructorStandingsWrapper> {
                TODO("Not yet implemented")
            }

            override fun getConstructorStandingsBySeason(season: String, constructorId: String): Result<ConstructorStandingsWrapper> {
                TODO("Not yet implemented")
            }

            override fun getAllConstructorStandingsById(constructorId: String): Result<MutableList<ConstructorStandingsWrapper>> {
                TODO("Not yet implemented")
            }

            override fun getCurrentRaceSchedule(): Result<List<Race>> {
                TODO("Not yet implemented")
            }

            override fun getDriverResultsByRace(race: Race): Result<List<DriverResult>> {
                TODO("Not yet implemented")
            }

            override fun getDriverStandingsBySeason(season: String, driverId: String): Result<DriverStandingsWrapper> {
                return when (repositoryStatus) {
                    RepositoryStatus.SUCCESS -> Success(givenADriverStandingsWrapper())
                    RepositoryStatus.FAILURE -> Error()
                }
            }
        }
    }
}