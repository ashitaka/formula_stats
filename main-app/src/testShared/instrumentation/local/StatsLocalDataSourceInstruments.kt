package instrumentation.local

import com.jorgenunezvazquez.formulastats.domain.datasources.cache.CacheDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData

enum class LocalDataSourceStatus {
    SUCCESS, FAILURE
}

object StatsLocalDataSourceInstruments {

    fun givenACountriesLocalDataSource(exception: Exception? = null) = object : CountriesLocalDataSource {

        override fun getCountryFlagUrlByCountry(country: String): String = exception?.let { throw Exception() } ?: "url"

        override fun getCountries(): List<CountryData> = mutableListOf()

        override fun saveCountries(countries: List<CountryData>) {}

        override fun getCountryFlagUrlByNationalityString(nationality: String): String = "url"
    }

    fun givenACacheDataSource(exception: Exception? = null) = object : CacheDataSource {

        override fun getImageUrl(wikiTitle: String): String = "imageUrl"

        override fun putImageUrl(wikiTitle: String, driverImageUrl: String) {}

        override fun isImageUrlCached(wikiTitle: String): Boolean = exception?.let { throw Exception() } ?: false
    }
}