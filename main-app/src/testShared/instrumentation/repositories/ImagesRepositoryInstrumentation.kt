package instrumentation.repositories

import com.jorgenunezvazquez.formulastats.data.repositories.ImagesRepositoryImpl
import instrumentation.local.StatsLocalDataSourceInstruments.givenACacheDataSource
import instrumentation.local.StatsLocalDataSourceInstruments.givenACountriesLocalDataSource
import instrumentation.remote.ImagesRemoteDataSourceInstruments.givenImagesRemoteDataSource

object ImagesRepositoryInstrumentation {

    fun givenAImagesRepository(): ImagesRepositoryImpl {
        return ImagesRepositoryImpl(
            givenACountriesLocalDataSource(),
            givenImagesRemoteDataSource(),
            givenACacheDataSource()
        )
    }
}
