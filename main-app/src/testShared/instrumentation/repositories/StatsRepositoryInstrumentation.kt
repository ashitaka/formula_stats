package instrumentation.repositories

import com.jorgenunezvazquez.formulastats.data.repositories.StatsRepositoryImpl
import instrumentation.remote.StatsRemoteDataSourceConfig
import instrumentation.remote.StatsRemoteDataSourceInstruments

object StatsRepositoryInstrumentation {

    fun givenAStatsRepository(statsRemoteDataSourceConfig: StatsRemoteDataSourceConfig = StatsRemoteDataSourceConfig()): StatsRepositoryImpl {
        return StatsRepositoryImpl(StatsRemoteDataSourceInstruments.givenStatsRemoteDataSource(statsRemoteDataSourceConfig))
    }
}