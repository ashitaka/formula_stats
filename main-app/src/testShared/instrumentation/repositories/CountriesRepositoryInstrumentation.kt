package instrumentation.repositories

import com.jorgenunezvazquez.formulastats.data.repositories.CountriesRepositoryImpl
import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.remote.CountriesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData

object CountriesRepositoryInstrumentation {

    fun givenACountriesRepository(exception: Exception? = null, returnLocalEmptyList: Boolean = false): CountriesRepositoryImpl {

        return CountriesRepositoryImpl(object : CountriesLocalDataSource {

            override fun getCountries(): MutableList<CountryData> = exception?.let { throw Exception() }
                ?: if (returnLocalEmptyList) mutableListOf() else mutableListOf(givenACountryData())

            override fun getCountryFlagUrlByCountry(country: String): String = exception?.let { throw Exception() }
                ?: "flag"

            override fun saveCountries(countries: List<CountryData>) {}

            override fun getCountryFlagUrlByNationalityString(nationality: String): String {
                TODO("Not yet implemented")
            }

        }, object : CountriesRemoteDataSource {
            override fun getCountries(): MutableList<CountryData> = exception?.let { mutableListOf<CountryData>() }
                ?: mutableListOf(givenACountryData())
        })
    }
}

fun givenACountryData() = CountryData("GZ", "Galiza", "Galego")