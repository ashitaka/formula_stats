package instrumentation.remote

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.ImagesRemoteDataSource

object ImagesRemoteDataSourceInstruments {

    fun givenImagesRemoteDataSource() = object : ImagesRemoteDataSource {

        override fun getImageFromWiki(pageTitle: String): String = "wiki image"
    }
}