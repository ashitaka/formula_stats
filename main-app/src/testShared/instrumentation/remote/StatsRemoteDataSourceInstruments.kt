package instrumentation.remote

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.StatsRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import instrumentation.domain.StatsInstrumentation.givenAConstructorsStandingsWrapper
import instrumentation.domain.StatsInstrumentation.givenADriverRaceResult
import instrumentation.domain.StatsInstrumentation.givenADriverResult
import instrumentation.domain.StatsInstrumentation.givenADriverStandingsWrapper
import instrumentation.domain.StatsInstrumentation.givenARace

enum class RemoteDataSourceStatus {
    SUCCESS, FAILURE
}

data class StatsRemoteDataSourceConfig(
    val statsRemoteDataSourceStatus: RemoteDataSourceStatus = RemoteDataSourceStatus.SUCCESS,
    val allDriverRaceResults: List<DriverRaceResult> = listOf(givenADriverRaceResult()),
    val driverChampionships: Int = 0,
    val driverResultsBySeason: List<DriverRaceResult> = listOf(givenADriverRaceResult()),
    val raceSchedule: List<Race> = listOf(givenARace()),
    val driverResultsByRace: List<DriverResult> = listOf(givenADriverResult()),
    val driverStandingsBySeason: DriverStandingsWrapper = givenADriverStandingsWrapper(),
    val driversSeasonStandings: DriverStandingsWrapper = givenADriverStandingsWrapper(),
    val constructorSeasonStandings: ConstructorStandingsWrapper = givenAConstructorsStandingsWrapper(),
    val allConstructorStandingsById: List<ConstructorStandingsWrapper> = listOf(givenAConstructorsStandingsWrapper()),
    val constructorStandingsBySeason: ConstructorStandingsWrapper = givenAConstructorsStandingsWrapper()
)

object StatsRemoteDataSourceInstruments {

    fun givenStatsRemoteDataSource(statsRemoteDataSourceConfig: StatsRemoteDataSourceConfig = StatsRemoteDataSourceConfig()) = object : StatsRemoteDataSource {

        override fun getAllDriverRaceResults(driverId: String): List<DriverRaceResult> = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.allDriverRaceResults
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getDriverChampionships(driverId: String): Int = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.driverChampionships
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getDriverResultsBySeason(season: String, driverId: String): List<DriverRaceResult> = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.driverResultsBySeason
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getCurrentRaceSchedule(): List<Race> = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.raceSchedule
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getDriverResultsByRace(race: Race): List<DriverResult> = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.driverResultsByRace
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getDriverStandingsBySeason(season: String, driverId: String): DriverStandingsWrapper = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.driverStandingsBySeason
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getDriversSeasonStandings(season: String): DriverStandingsWrapper = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.driversSeasonStandings
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getConstructorSeasonStandings(): ConstructorStandingsWrapper = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.constructorSeasonStandings
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getAllConstructorStandingsById(constructorId: String): List<ConstructorStandingsWrapper> = when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
            RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.allConstructorStandingsById
            RemoteDataSourceStatus.FAILURE -> throw Exception()
        }

        override fun getConstructorStandingsBySeason(season: String, constructorId: String): ConstructorStandingsWrapper =
            when (statsRemoteDataSourceConfig.statsRemoteDataSourceStatus) {
                RemoteDataSourceStatus.SUCCESS -> statsRemoteDataSourceConfig.constructorStandingsBySeason
                RemoteDataSourceStatus.FAILURE -> throw Exception()
            }
    }
}