package com.jorgenunezvazquez.formulastats.domain.useCases.drivers

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.StringUtils
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository


class GetDriversCurrentSeasonStandingsUseCase(private val repository: StatsRepository, private val imagesRepository: ImagesRepository) : BaseUseCase<Unit, DriverStandingsWrapper>() {

    override fun run(): Result<DriverStandingsWrapper> {
        val driverSeasonStandingsResult = repository.getDriverSeasonStandings()
        if (driverSeasonStandingsResult is Success) {
            driverSeasonStandingsResult.data.driverStandings.forEach { driverStanding ->
                driverStanding.driver.flagUrl = (imagesRepository.getFlagByNationality(driverStanding.driver.nationality) as? Success)?.data.orEmpty()
                driverStanding.driver.imageUrl = (imagesRepository.getImageUrlFromWiki(StringUtils.getLastBitFromUrl(driverStanding.driver.url)) as? Success)?.data.orEmpty()
            }
        }
        return driverSeasonStandingsResult
    }
}

