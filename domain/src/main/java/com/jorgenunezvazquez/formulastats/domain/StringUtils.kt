package com.jorgenunezvazquez.formulastats.domain

object StringUtils {

    fun getLastBitFromUrl(url: String): String {
        return url.replaceFirst(".*/([^/?]+).*".toRegex(), "$1")
    }
    //TODO REVIEW THIS
}