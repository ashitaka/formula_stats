package com.jorgenunezvazquez.formulastats.domain.invoker


sealed class Result<out T>

data class Success<T>(val data: T) : Result<T>()
data class Error(val error: Exception = Exception(), val message: String? = null) : Result<Nothing>()


