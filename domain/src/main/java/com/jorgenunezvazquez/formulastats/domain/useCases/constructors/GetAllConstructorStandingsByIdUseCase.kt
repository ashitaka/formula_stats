package com.jorgenunezvazquez.formulastats.domain.useCases.constructors

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository

class GetAllConstructorStandingsByIdUseCase(private val statsRepository: StatsRepository) : BaseUseCase<String, List<ConstructorStandingsWrapper>>() {

    override fun run(): Result<List<ConstructorStandingsWrapper>> {
        return params?.let { statsRepository.getAllConstructorStandingsById(it) } ?: Error()
    }
}