package com.jorgenunezvazquez.formulastats.domain.model.constructors

import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import java.io.Serializable

class ConstructorStandings(
    val position: String,
    val points: String,
    val wins: String,
    val constructor: Constructor,
    var driverStandings: List<DriverStandings> = emptyList()
) : Serializable

data class ConstructorStandingsWrapper(
    val season: String,
    val round: String,
    val constructorStandings: List<ConstructorStandings>,
) : Serializable

