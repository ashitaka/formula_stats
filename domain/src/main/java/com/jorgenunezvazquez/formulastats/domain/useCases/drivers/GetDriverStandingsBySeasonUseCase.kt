package com.jorgenunezvazquez.formulastats.domain.useCases.drivers

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import com.jorgenunezvazquez.formulastats.domain.invoker.*


class GetDriverStandingsBySeasonUseCase(private val repository: StatsRepository) : BaseUseCase<DriverStandingsParams, DriverStandingsWrapper>() {

    override fun run(): Result<DriverStandingsWrapper> {
        params?.let { return repository.getDriverStandingsBySeason(it.season, it.driverId) } ?: return Error()
    }
}

data class DriverStandingsParams(val season: String, val driverId: String)
