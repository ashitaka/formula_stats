package com.jorgenunezvazquez.formulastats.domain

import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import kotlinx.coroutines.*

typealias Callback<R> = (Result<R>) -> Unit

class UseCaseInvoker : Invoker {

    private val scope = CoroutineScope(Job() + Dispatchers.Main)

    override fun <P, R> execute(useCase: BaseUseCase<P, R>, callback: Callback<R>?) {
        scope.launch {
            val useCaseResult = executeInBackground(useCase)
            callback?.invoke(useCaseResult)
        }
    }

    private suspend fun <R> executeInBackground(baseUseCase: BaseUseCase<*, R>) = withContext(Dispatchers.IO) {
        baseUseCase.run()
    }

    override fun cancelTasks() {
        scope.coroutineContext.cancelChildren()
    }
}


