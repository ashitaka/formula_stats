package com.jorgenunezvazquez.formulastats.domain.datasources.cache

interface CacheDataSource {

    @Throws(Exception::class)
    fun getImageUrl(wikiTitle: String): String

    @Throws(Exception::class)
    fun putImageUrl(wikiTitle: String, driverImageUrl: String)

    @Throws(Exception::class)
    fun isImageUrlCached(wikiTitle: String): Boolean
}