package com.jorgenunezvazquez.formulastats.domain

interface Invoker {
    fun <P, R> execute(useCase: BaseUseCase<P, R>, callback: Callback<R>? = null)
    fun cancelTasks()
}