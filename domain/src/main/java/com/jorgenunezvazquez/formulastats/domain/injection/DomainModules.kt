package com.jorgenunezvazquez.formulastats.domain.injection

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.UseCaseInvoker
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetAllConstructorStandingsByIdUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetConstructorDriversResultsBySeasonUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetConstructorSeasonStandingsUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetAllDriverResultsByIdUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriverStandingsBySeasonUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriversCurrentSeasonStandingsUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.main.LoadCountriesUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.races.GetCurrentRaceScheduleUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.races.GetDriverResultsByRaceUseCase
import org.koin.dsl.module

object DomainModules {

    val domainModule = module {
        factory { GetDriverStandingsBySeasonUseCase(get()) }
        factory { LoadCountriesUseCase(get()) }
        factory { GetDriversCurrentSeasonStandingsUseCase(get(), get()) }
        factory { GetAllDriverResultsByIdUseCase(get()) }
        factory { GetConstructorSeasonStandingsUseCase(get(), get()) }
        factory { GetAllConstructorStandingsByIdUseCase(get()) }
        factory { GetConstructorDriversResultsBySeasonUseCase(get(), get()) }
        factory { GetCurrentRaceScheduleUseCase(get(), get()) }
        factory { GetDriverResultsByRaceUseCase(get(), get()) }
        factory<Invoker> { UseCaseInvoker() }
    }
}