package com.jorgenunezvazquez.formulastats.domain.useCases.constructors

import com.jorgenunezvazquez.formulastats.domain.StringUtils
import com.jorgenunezvazquez.formulastats.domain.invoker.*
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository

class GetConstructorDriversResultsBySeasonUseCase(private val statsRepository: StatsRepository, private val imagesRepository: ImagesRepository) :
    com.jorgenunezvazquez.formulastats.domain.BaseUseCase<GetConstructorDriverResultsParams, ConstructorSeasonDriversWrapper>() {

    override fun run(): Result<ConstructorSeasonDriversWrapper> {
        return params?.let { useCaseParams ->
            val constructorSeasonStandings = statsRepository.getConstructorStandingsBySeason(useCaseParams.season, useCaseParams.constructorId)
            val driverSeasonStandingsResult = statsRepository.getDriverSeasonStandings(useCaseParams.season)
            if (constructorSeasonStandings is Success && constructorSeasonStandings.data.constructorStandings.isNotEmpty() && driverSeasonStandingsResult is Success) {
                val constructorDriverIds = driverSeasonStandingsResult.data.driverStandings.filter {
                    it.constructors.any { constructor -> constructor.constructorId == useCaseParams.constructorId }
                }.map { it.driver.driverId }

                val driverStandingsList = mutableListOf<DriverStandings>()
                val driverRaceResultList = mutableListOf<List<DriverRaceResult>>()

                constructorDriverIds.forEach { driverId ->
                    val driverRaceResult = statsRepository.getDriverResultsBySeason(useCaseParams.season, driverId)
                    if (driverRaceResult is Success && driverRaceResult.data.isNotEmpty()) {
                        driverRaceResultList.add(driverRaceResult.data)
                        val driverSeasonStandings = getDriverSeasonStandings(driverSeasonStandingsResult.data, driverId)
                        driverSeasonStandings.driver.imageUrl = getDriverImageUrl(driverRaceResult.data)
                        driverStandingsList.add(driverSeasonStandings)
                    } else {
                        Error()
                    }
                }
                Success(
                    ConstructorSeasonDriversWrapper(
                        constructorSeasonStandings.data.round,
                        constructorSeasonStandings.data.constructorStandings.first(),
                        driverStandingsList,
                        driverRaceResultList
                    )
                )
            } else {
                Error()
            }
        } ?: Error()
    }

    private fun getDriverImageUrl(driverRaceResultList: List<DriverRaceResult>): String {
        val driverUrl = driverRaceResultList.first().results.driver.url
        return (imagesRepository.getImageUrlFromWiki(StringUtils.getLastBitFromUrl(driverUrl)) as? Success)?.data.orEmpty()
    }

    private fun getDriverSeasonStandings(driverStandingsWrapper: DriverStandingsWrapper, driverId: String): DriverStandings {
        return driverStandingsWrapper.driverStandings.first { it.driver.driverId == driverId }
    }
}

data class GetConstructorDriverResultsParams(val season: String, val constructorId: String)

data class ConstructorSeasonDriversWrapper(
    val round: String,
    val constructorSeasonStandings: ConstructorStandings,
    val driverStandings: List<DriverStandings>,
    val driverSeasonResult: List<List<DriverRaceResult>>
)