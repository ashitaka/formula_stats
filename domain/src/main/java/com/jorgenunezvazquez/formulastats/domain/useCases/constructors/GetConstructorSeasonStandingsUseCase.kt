package com.jorgenunezvazquez.formulastats.domain.useCases.constructors


import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.StringUtils
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository


class GetConstructorSeasonStandingsUseCase(
    private val statsRepository: StatsRepository,
    private val imagesRepository: ImagesRepository
) : BaseUseCase<Unit, ConstructorStandingsWrapper>() {

    override fun run(): Result<ConstructorStandingsWrapper> {
        val constructorStandingsResult = statsRepository.getConstructorSeasonStandings()
        return if (constructorStandingsResult is Success) {
            val seasonDriverStandings = (statsRepository.getDriverSeasonStandings() as? Success)?.data?.driverStandings ?: emptyList()
            constructorStandingsResult.data.constructorStandings.forEach { constructorStanding ->
                constructorStanding.constructor.flagUrl = (imagesRepository.getFlagByNationality(constructorStanding.constructor.nationality) as? Success)?.data.orEmpty()
                constructorStanding.driverStandings = seasonDriverStandings.filter {
                    it.constructors.any { driverConstructor -> driverConstructor.constructorId == constructorStanding.constructor.constructorId }
                }.onEach { driverStandings ->
                    driverStandings.driver.imageUrl =
                        (imagesRepository.getImageUrlFromWiki(StringUtils.getLastBitFromUrl(driverStandings.driver.url)) as? Success)?.data.orEmpty()
                }
            }
            constructorStandingsResult
        } else {
            Error()
        }
    }
}

