package com.jorgenunezvazquez.formulastats.domain.datasources.remote

interface ImagesRemoteDataSource {
    fun getImageFromWiki(pageTitle: String): String
}