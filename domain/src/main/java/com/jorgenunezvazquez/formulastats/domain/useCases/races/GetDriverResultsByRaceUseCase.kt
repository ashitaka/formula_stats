package com.jorgenunezvazquez.formulastats.domain.useCases.races

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.StringUtils
import com.jorgenunezvazquez.formulastats.domain.invoker.*
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.drivers.Driver
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.model.races.RaceResultsWrapper
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository

private const val WINNER_POS = "1"

class GetDriverResultsByRaceUseCase(
    private val statsRepository: StatsRepository,
    private val imagesRepository: ImagesRepository
) : BaseUseCase<GetDriverResultsByRaceParams, RaceResultsWrapper>() {

    override fun run(): Result<RaceResultsWrapper> {
        return params?.race?.let { race ->
            val driverResultsByRaceResult = statsRepository.getDriverResultsByRace(race)
            if (driverResultsByRaceResult is Success) {
                val winnerDriverUrl = getWinner(driverResultsByRaceResult.data)?.url.orEmpty()
                val winnerImage = (imagesRepository.getImageUrlFromWiki(StringUtils.getLastBitFromUrl(winnerDriverUrl)) as? Success)?.data
                Success(RaceResultsWrapper(driverResults = driverResultsByRaceResult.data, winnerImageUrl = winnerImage))
            } else {
                Error()
            }
        } ?: Error()
    }

    private fun getWinner(driverResultList: List<DriverResult>?): Driver? {
        return driverResultList?.find { it.position == WINNER_POS }?.driver
    }
}

data class GetDriverResultsByRaceParams(val race: Race)