package com.jorgenunezvazquez.formulastats.domain.datasources.local

import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData


interface CountriesLocalDataSource {

    fun getCountryFlagUrlByNationalityString(nationality: String): String

    fun getCountryFlagUrlByCountry(country: String): String

    fun getCountries(): List<CountryData>

    fun saveCountries(countries: List<CountryData>)
}