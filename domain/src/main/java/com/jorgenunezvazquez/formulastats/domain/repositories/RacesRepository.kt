package com.jorgenunezvazquez.formulastats.domain.repositories

import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.races.Race

interface RacesRepository {
    fun getCurrentRaceSchedule(): Result<List<Race>>
    fun getDriverResultsByRace(race: Race): Result<List<DriverResult>>
}