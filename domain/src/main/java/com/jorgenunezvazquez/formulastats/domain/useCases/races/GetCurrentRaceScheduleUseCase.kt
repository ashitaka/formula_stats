package com.jorgenunezvazquez.formulastats.domain.useCases.races

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.StringUtils
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository

class GetCurrentRaceScheduleUseCase(
    private val statsRepository: StatsRepository,
    private val imagesRepository: ImagesRepository
) : BaseUseCase<Unit, List<Race>>() {

    override fun run(): Result<List<Race>> {
        val racesResult = statsRepository.getCurrentRaceSchedule()
        if (racesResult is Success) {
            racesResult.data.forEach { race ->
                race.imageUrl = (imagesRepository.getImageUrlFromWiki(StringUtils.getLastBitFromUrl(race.circuit.url)) as? Success)?.data.orEmpty()
                race.flagUrl = (imagesRepository.getFlagByCountry(race.circuit.location.country) as? Success)?.data.orEmpty()
            }
        }

        return racesResult
    }
}