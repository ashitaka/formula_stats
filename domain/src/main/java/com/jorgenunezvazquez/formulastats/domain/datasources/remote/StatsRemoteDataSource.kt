package com.jorgenunezvazquez.formulastats.domain.datasources.remote

import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.races.Race


interface StatsRemoteDataSource {
    fun getCurrentRaceSchedule(): List<Race>
    fun getDriversSeasonStandings(season: String): DriverStandingsWrapper
    fun getConstructorSeasonStandings(): ConstructorStandingsWrapper
    fun getDriverResultsByRace(race: Race): List<DriverResult>
    fun getDriverStandingsBySeason(season: String, driverId: String): DriverStandingsWrapper
    fun getAllDriverRaceResults(driverId: String): List<DriverRaceResult>
    fun getDriverChampionships(driverId: String): Int
    fun getDriverResultsBySeason(season: String, driverId: String): List<DriverRaceResult>
    fun getAllConstructorStandingsById(constructorId: String): List<ConstructorStandingsWrapper>
    fun getConstructorStandingsBySeason(season: String, constructorId: String): ConstructorStandingsWrapper
}