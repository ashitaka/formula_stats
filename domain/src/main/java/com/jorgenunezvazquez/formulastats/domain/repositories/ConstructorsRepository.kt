package com.jorgenunezvazquez.formulastats.domain.repositories

import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper

interface ConstructorsRepository {
    fun getConstructorSeasonStandings(): Result<ConstructorStandingsWrapper>
    fun getAllConstructorStandingsById(constructorId: String): Result<MutableList<ConstructorStandingsWrapper>>
}