package com.jorgenunezvazquez.formulastats.domain

import com.jorgenunezvazquez.formulastats.domain.invoker.Result

abstract class BaseUseCase<P, R> {

    protected var params: P? = null

    abstract fun run(): Result<R>

    infix fun withParams(params: P) = also {
        this.params = params
    }
}

