package com.jorgenunezvazquez.formulastats.domain.repositories

import com.jorgenunezvazquez.formulastats.domain.invoker.Result

interface ImagesRepository {

    fun getImageUrlFromWiki(pageTitle: String): Result<String>

    fun getFlagByNationality(nationality: String): Result<String>

    fun getFlagByCountry(country: String): Result<String>
}