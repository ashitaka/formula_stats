package com.jorgenunezvazquez.formulastats.domain.model.countries

data class CountryData(
    val name: String,
    val alpha2Code: String,
    val demonym: String
)