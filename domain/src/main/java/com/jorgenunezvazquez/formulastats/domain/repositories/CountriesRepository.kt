package com.jorgenunezvazquez.formulastats.domain.repositories

import com.jorgenunezvazquez.formulastats.domain.invoker.Result

interface CountriesRepository {
    fun loadCountries(): Result<Unit>
}