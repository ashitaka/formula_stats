package com.jorgenunezvazquez.formulastats.domain.model.drivers

import com.jorgenunezvazquez.formulastats.domain.model.constructors.Constructor
import java.io.Serializable

data class DriverStandings(
        val position: String,
        val positionText: String,
        val points: String,
        val wins: String,
        val driver: Driver,
        val constructors: List<Constructor>
) : Serializable

data class DriverStandingsWrapper(
        val season: String,
        val round: String,
        val driverStandings: List<DriverStandings>
) : Serializable