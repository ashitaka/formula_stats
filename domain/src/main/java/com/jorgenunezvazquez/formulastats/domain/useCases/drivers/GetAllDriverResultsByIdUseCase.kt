package com.jorgenunezvazquez.formulastats.domain.useCases.drivers

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverTotalResult
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import com.jorgenunezvazquez.formulastats.domain.invoker.*


class GetAllDriverResultsByIdUseCase(private val repository: StatsRepository) : BaseUseCase<String, DriverTotalResult>() {

    override fun run(): Result<DriverTotalResult> {
        params?.let { return repository.getDriverRaceResults(it) } ?: return Error()
    }
}
