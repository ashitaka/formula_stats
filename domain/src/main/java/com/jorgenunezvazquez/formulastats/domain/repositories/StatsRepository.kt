package com.jorgenunezvazquez.formulastats.domain.repositories

import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverTotalResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.invoker.Result

interface StatsRepository {
    fun getCurrentRaceSchedule(): Result<List<Race>>
    fun getDriverResultsByRace(race: Race): Result<List<DriverResult>>
    fun getDriverStandingsBySeason(season: String, driverId: String): Result<DriverStandingsWrapper>
    fun getDriverRaceResults(driverId:String): Result<DriverTotalResult>
    fun getDriverSeasonStandings(season: String = "current"): Result<DriverStandingsWrapper>
    fun getDriverResultsBySeason(season: String, driverId: String): Result<List<DriverRaceResult>>
    fun getConstructorSeasonStandings(): Result<ConstructorStandingsWrapper>
    fun getConstructorStandingsBySeason(season: String, constructorId: String): Result<ConstructorStandingsWrapper>
    fun getAllConstructorStandingsById(constructorId: String): Result<List<ConstructorStandingsWrapper>>
}