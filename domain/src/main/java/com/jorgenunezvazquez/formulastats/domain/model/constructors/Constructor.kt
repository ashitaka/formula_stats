package com.jorgenunezvazquez.formulastats.domain.model.constructors

import java.io.Serializable


data class Constructor(
    val constructorId: String,
    val url: String,
    val name: String,
    val nationality: String,
    var flagUrl: String = ""
) : Serializable