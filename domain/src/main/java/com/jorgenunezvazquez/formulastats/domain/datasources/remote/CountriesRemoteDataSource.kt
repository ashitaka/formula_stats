package com.jorgenunezvazquez.formulastats.domain.datasources.remote

import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData

interface CountriesRemoteDataSource {

    @Throws(Exception::class)
    fun getCountries(): List<CountryData>

}