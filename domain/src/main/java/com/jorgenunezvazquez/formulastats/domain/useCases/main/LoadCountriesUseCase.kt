package com.jorgenunezvazquez.formulastats.domain.useCases.main

import com.jorgenunezvazquez.formulastats.domain.BaseUseCase
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.repositories.CountriesRepository

class LoadCountriesUseCase(private val repository: CountriesRepository) : BaseUseCase<Void, Unit>() {

    override fun run(): Result<Unit> {
        return repository.loadCountries()
    }
}
