package com.jorgenunezvazquez.formulastats.domain.model.drivers

import java.io.Serializable

data class Driver(
        val driverId: String = "",
        val permanentNumber: String = "",
        val code: String = "",
        val url: String = "",
        val givenName: String = "",
        val familyName: String = "",
        val dateOfBirth: String = "",
        val nationality: String = "",
        var imageUrl: String = "",
        var flagUrl: String = ""
) : Serializable