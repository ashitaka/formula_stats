package com.jorgenunezvazquez.formulastats.domain

import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun getRaceOriginalDate(race: Race): Date? {
        return if (race.time != null) {
            getRaceDateTime(race)
        } else {
            getRaceDate(race)
        }
    }

    private fun getRaceDate(race: Race): Date? {
        val currentFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("GMT")
        }
        return currentFormat.parse(race.date)
    }

    private fun getRaceDateTime(race: Race): Date? {
        val currentFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).apply {
                timeZone = TimeZone.getTimeZone("GMT")
            }
        val raceDateTime = race.date + "T" + race.time
        return currentFormat.parse(raceDateTime)
    }
}