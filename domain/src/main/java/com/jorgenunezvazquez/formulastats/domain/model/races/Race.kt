package com.jorgenunezvazquez.formulastats.domain.model.races

import com.jorgenunezvazquez.formulastats.domain.model.drivers.Circuit
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import java.io.Serializable

data class RaceResultsWrapper(
    val driverResults: List<DriverResult>,
    val winnerImageUrl: String?
)

data class Race(
    val season: String,
    val round: String,
    val url: String,
    val raceName: String,
    val circuit: Circuit,
    val date: String,
    val time: String?,
    val driverResults: List<DriverResult>,
    var imageUrl: String? = null,
    var flagUrl: String? = null
) : Serializable


