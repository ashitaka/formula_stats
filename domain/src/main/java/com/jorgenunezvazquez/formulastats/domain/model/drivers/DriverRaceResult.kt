package com.jorgenunezvazquez.formulastats.domain.model.drivers

import com.jorgenunezvazquez.formulastats.domain.model.constructors.Constructor
import java.io.Serializable


data class DriverTotalResult(
    val titles: String,
    val results: List<DriverRaceResult>
)

data class DriverRaceResult(
    val season: String,
    val round: String,
    val url: String,
    val raceName: String,
    val circuit: Circuit,
    val date: String,
    val results: DriverResult
)

data class Circuit(
    val circuitId: String = "",
    val url: String = "",
    val circuitName: String = "",
    val location: Location
) : Serializable

data class Location(
    val lat: String = "",
    val long: String = "",
    val locality: String = "",
    val country: String = ""
) : Serializable

data class DriverResult(
    val number: String,
    val position: String,
    val points: String,
    val driver: Driver,
    val constructor: Constructor,
    val grid: String,
    val laps: String,
    val status: String,
    val time: Time?,
    val fastestLap: FastestLap?
) : Serializable

data class Time(
    val millis: String,
    val time: String
)

data class FastestLap(
    val rank: String,
    val lap: String,
    val time: FastestLapTime,
    val averageSpeed: AverageSpeed
)

data class FastestLapTime(
    val time: String
)

data class AverageSpeed(
    val units: String,
    val speed: String
)