package com.jorgenunezvazquez.formulastats.presentation

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.jorgenunezvazquez.formulastats.presentation.R
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

object ConstructorMarkerUtils : KoinComponent {

    fun getConstructorMarker(constructorId: String): Drawable? {
        val context: Context by inject()
        val markerStyle = ConstructorStyle.getValueById(constructorId)
        val wrapper = ContextThemeWrapper(context, markerStyle.style)
        return context.resources?.let { ResourcesCompat.getDrawable(it, R.drawable.ic_team_marker, wrapper.theme) }
    }
}