package com.jorgenunezvazquez.formulastats.presentation.utils

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.jorgenunezvazquez.formulastats.presentation.R
import java.util.*

object ChartUtils {

    private val colorList = listOf(Color.YELLOW, Color.CYAN, Color.GREEN, Color.MAGENTA)

    fun setChart(
        driverDataSetList: List<LineDataSet>,
        chart: LineChart,
        axisLeftInverted: Boolean
    ) {

        driverDataSetList.forEachIndexed { index, driverDataSet ->
            driverDataSet.color = colorList.getOrElse(index) {
                val random = Random()
                Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
            }
            driverDataSet.setDrawValues(false)
        }
        val lineData = LineData(driverDataSetList)
        chart.data = lineData
        styleChart(axisLeftInverted, chart)
    }

    fun styleChart(axisLeftInverted: Boolean, lineChart: LineChart) {
        lineChart.apply {
            xAxis.position = XAxis.XAxisPosition.TOP
            xAxis.textColor = ContextCompat.getColor(context, R.color.light_gray)
            xAxis.typeface = ResourcesCompat.getFont(context, R.font.josefin_md)
            xAxis.setDrawGridLines(false)
            xAxis.granularity = 1f

            axisLeft.textColor = ContextCompat.getColor(context, R.color.light_gray)
            axisLeft.typeface = ResourcesCompat.getFont(context, R.font.josefin_md)
            axisLeft.isInverted = axisLeftInverted
            axisLeft.setDrawGridLines(false)
            axisLeft.granularity = 1f
            axisLeft.axisMinimum = 1f
            axisLeft.spaceMin = 16f

            axisRight.isEnabled = false
            setTouchEnabled(false)
            description = Description().also { it.text = "" }

            legend.apply {
                textColor = ContextCompat.getColor(context, R.color.light_gray)
                typeface = ResourcesCompat.getFont(context, R.font.josefin_md)
                textSize = 12f
                horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
                verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
                xEntrySpace = 12f
                extraBottomOffset = 12f
            }

            animateXY(1000, 1000)
        }
    }
}