package com.jorgenunezvazquez.formulastats.presentation.main

import android.os.Bundle
import android.view.MenuItem
import androidx.core.splashscreen.SplashScreen
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import com.google.android.material.navigation.NavigationBarView
import com.jorgenunezvazquez.formulastats.presentation.BaseActivity
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.databinding.ActivityMainBinding
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : BaseActivity<MainPresenter>(), NavigationBarView.OnItemSelectedListener, MainViewContract {

    override val presenter: MainPresenter by inject { parametersOf(this) }

    private lateinit var binding: ActivityMainBinding
    private lateinit var customSplashScreen: SplashScreen

    override fun onCreate(savedInstanceState: Bundle?) {
        customSplashScreen = installSplashScreen()
        customSplashScreen.setKeepOnScreenCondition { true }
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.myToolbar)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val titleRes = when (item.itemId) {
            R.id.racesFragment -> R.string.race_schedule
            R.id.driversFragment -> R.string.drivers_title
            R.id.constructorsFragment -> R.string.constructors_title
            else -> R.string.app_name
        }
        supportActionBar?.setTitle(titleRes)

        val navController = findNavController(R.id.nav_host_fragment)
        return item.onNavDestinationSelected(navController)
    }

    override fun setupView() {
        binding.bottomNavigationView.setOnItemSelectedListener(this)
        customSplashScreen.setKeepOnScreenCondition { false }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}

