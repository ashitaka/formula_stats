package com.jorgenunezvazquez.formulastats.presentation.drivers

import android.content.Context
import com.jorgenunezvazquez.formulastats.domain.model.drivers.Driver
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverTotalResult
import com.jorgenunezvazquez.formulastats.presentation.R
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

private const val FIRST_POSITION_ID = "1"
private const val SECOND_POSITION_ID = "2"
private const val THIRD_POSITION_ID = "3"

data class DriverDataModel(
    val flagImageUrl: String,
    val driverImageUrl: String,
    val driverNationality: String,
    val driverNumber: String,
    val driverCode: String,
    val driverAge: String,
    val driverId: String,
    val driverName: String
)

data class DriverTotalStatsModel(
    val titles: String,
    val totalWins: String,
    val totalPodiums: String,
    val totalPoles: String,
    val totalFastestLap: String,
    val bestPosition: String,
    val totalSeasons: String,
    val totalPoints: String,
    val seasonList: List<String>
)

data class DriverSeasonStatsModel(
    val seasonPosition: String,
    val seasonTeam: String,
    val seasonWins: String,
    val seasonPoints: String,
    val seasonTitle: String
)

data class DriverSeasonChartsModel(
    val rounds: List<Float>,
    val pointsChartTitle: String,
    val resultsChartTitle: String,
    val seasonGrids: List<Float>,
    val seasonGridRounds: List<Float>,
    val seasonPositions: List<Float>,
    val seasonPoints: List<Float>
)

object DriverDetailsModelHelper : KoinComponent {

    fun getDriverDataModel(driverStandings: DriverStandings): DriverDataModel {
        return with(driverStandings.driver) {
            DriverDataModel(
                flagImageUrl = flagUrl,
                driverImageUrl = imageUrl,
                driverNationality = nationality,
                driverNumber = permanentNumber,
                driverCode = code,
                driverAge = getDriverAge(driver = driverStandings.driver),
                driverId = driverId,
                driverName = getDriverName(this)
            )
        }
    }

    private fun getDriverAge(driver: Driver): String = try {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        format.parse(driver.dateOfBirth)?.let { driverDate ->
            val today = Calendar.getInstance()
            val driverDateOfBirth = Calendar.getInstance().apply { time = driverDate }
            var age = today.get(Calendar.YEAR) - driverDateOfBirth.get(Calendar.YEAR)
            if (today.get(Calendar.DAY_OF_YEAR) < driverDateOfBirth.get(Calendar.DAY_OF_YEAR)) {
                age = age.dec()
            }
            age.toString()
        } ?: "-"
    } catch (e: ParseException) {
        "-"
    }

    fun getDriverSeasonStatsModel(
        driverStandings: DriverStandings,
        season: String
    ): DriverSeasonStatsModel {
        val context: Context by inject()
        return with(driverStandings) {
            DriverSeasonStatsModel(
                seasonPosition = position,
                seasonTeam = constructors.joinToString(", ") { it.name },
                seasonWins = wins,
                seasonPoints = points,
                seasonTitle = String.format(
                    context.resources.getString(R.string.season_title),
                    season
                )
            )
        }
    }

    fun getDriverSeasonChartsModel(
        driverResults: List<DriverRaceResult>,
        round: String
    ): DriverSeasonChartsModel? {
        val context: Context by inject()
        return try {
            with(driverResults) {
                val validGridResults = filter { it.results.grid.toFloat() > 0 }
                DriverSeasonChartsModel(
                    rounds = map { it.round.toFloat() },
                    pointsChartTitle = String.format(
                        context.resources.getString(R.string.points_round),
                        round
                    ),
                    resultsChartTitle = String.format(
                        context.resources.getString(R.string.results),
                        round
                    ),
                    seasonGrids = validGridResults.map { it.results.grid.toFloat() },
                    seasonGridRounds = validGridResults.map { it.round.toFloat() },
                    seasonPoints = map { it.results.points.toFloat() },
                    seasonPositions = map { it.results.position.toFloat() }
                )
            }
        } catch (e: Exception) {
            null
        }
    }

    fun getDriverTotalStatsModel(driverTotalResult: DriverTotalResult): DriverTotalStatsModel {
        with(driverTotalResult.results) {
            return DriverTotalStatsModel(
                totalWins = getDriverWins(this),
                totalPodiums = getDriverPodiums(this),
                totalPoles = getDriverPoles(this),
                totalFastestLap = getDriverFastestLaps(this),
                bestPosition = getBestDriverPosition(this),
                totalSeasons = getDriverSeasons(this),
                totalPoints = getDriverPoints(this),
                titles = driverTotalResult.titles,
                seasonList = getDriverSeasonList(this)
            )
        }
    }

    fun getDriverName(driver: Driver): String {
        return String.format("%s, %s", driver.familyName, driver.givenName)
        /*val span = SpannableString(text)
        span.setSpan(RelativeSizeSpan(0.8f), text.length - driver.givenName.length, text.length, 0)
        return span.toString()*/
        //TODO REVIEW SPAN
    }

    private fun getDriverWins(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.filter { it.results.position == FIRST_POSITION_ID }.count()
            .toString()
    }

    private fun getDriverPodiums(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.filter {
            val position = it.results.position
            position == FIRST_POSITION_ID || position == SECOND_POSITION_ID || position == THIRD_POSITION_ID
        }.count().toString()
    }

    private fun getDriverPoles(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.filter { it.results.grid == FIRST_POSITION_ID }.count()
            .toString()
    }

    private fun getDriverFastestLaps(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.filter { it.results.fastestLap?.rank == FIRST_POSITION_ID }
            .count().toString()
    }

    private fun getDriverPoints(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.sumOf { it.results.points.toDouble() }.toString()
            .removeSuffix(".0")
    }

    private fun getBestDriverPosition(driverRaceResultList: List<DriverRaceResult>): String {
        return driverRaceResultList.minByOrNull { it.results.position.toInt() }?.results?.position.toString()
    }

    private fun getDriverSeasons(driverRaceResultList: List<DriverRaceResult>): String {
        return getDriverSeasonList(driverRaceResultList).size.toString()
    }

    private fun getDriverSeasonList(driverRaceResultList: List<DriverRaceResult>): List<String> {
        return driverRaceResultList.map { it.season }.toSet().toList()
    }
}