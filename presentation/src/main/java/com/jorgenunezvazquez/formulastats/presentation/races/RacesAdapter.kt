package com.jorgenunezvazquez.formulastats.presentation.races

import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemFooterBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemRacesBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.RaceDetailGpInfoItemBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.visible
import com.squareup.picasso.Picasso

class RacesAdapter(
    private val raceScheduleList: List<AdapterItemModel>,
    private val onRaceClickListener: (String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            RaceDetailItemViewType.GP_INFO_ITEM.viewType -> createNextGPInfoViewHolder(parent)
            RaceItemViewType.GP_SCHEDULE_INFO_ITEM.viewType -> createGPScheduleInfoViewHolder(parent)
            RaceItemViewType.FOOTER_ITEM.viewType -> createFooterViewHolder(parent)
            else -> throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NextGPInfoViewHolder -> holder.bind(
                raceScheduleList[position] as GPInfoModel,
                onRaceClickListener
            )

            is RaceViewHolder -> holder.bindRace(
                raceScheduleList[position] as RaceModel,
                onRaceClickListener
            )

            is FooterViewHolder -> holder.bind(raceScheduleList[position] as FooterModel)
        }
    }

    override fun getItemCount(): Int {
        return raceScheduleList.size
    }

    override fun getItemViewType(position: Int): Int {
        return raceScheduleList[position].getItemViewType()
    }

    override fun getItemId(position: Int): Long {
        return raceScheduleList[position].getItemViewId()
    }

    private fun createNextGPInfoViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return NextGPInfoViewHolder(
            RaceDetailGpInfoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createGPScheduleInfoViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return RaceViewHolder(
            ItemRacesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createFooterViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return FooterViewHolder(
            ItemFooterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class RaceViewHolder internal constructor(private val viewBinding: ItemRacesBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindRace(raceModel: RaceModel, onRaceClickListener: (String) -> Unit) {
            with(viewBinding) {
                raceName.text = raceModel.raceName
                raceCircuit.text = raceModel.raceCircuit
                raceOrder.text = raceModel.raceOrder
                raceLocation.text = raceModel.raceLocation
                raceDate.text = raceModel.raceDate

                if (raceModel.raceFlagUrl.isNotEmpty()) {
                    Picasso.get().load(raceModel.raceFlagUrl).fit().centerCrop().into(raceCountry)
                }
                raceItem.setOnClickListener {
                    onRaceClickListener(raceModel.raceName)
                }
            }
        }
    }

    class NextGPInfoViewHolder internal constructor(private val viewBinding: RaceDetailGpInfoItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(nextGPInfoModel: GPInfoModel, onRaceClickListener: (String) -> Unit) {
            with(viewBinding) {
                if (nextGPInfoModel.raceImage.isNotEmpty()) {
                    Picasso.get().load(nextGPInfoModel.raceImage).fit().centerInside()
                        .into(raceImageItem)
                }

                if (nextGPInfoModel.flagImage.isNotEmpty()) {
                    Picasso.get().load(nextGPInfoModel.flagImage).fit().centerCrop().into(raceFlag)
                }
                raceName.text = nextGPInfoModel.raceName
                circuitName.text = nextGPInfoModel.circuitName
                locality.text = nextGPInfoModel.locality
                date.text = nextGPInfoModel.raceDate
                time.text = nextGPInfoModel.raceTime
                raceImageItem.background = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.next_gp_gradient_background
                )
                raceImageItem.setOnClickListener {
                    onRaceClickListener(nextGPInfoModel.raceName)
                }
                nextRaceViewGroup.visible()
            }
        }
    }

    class FooterViewHolder internal constructor(private val viewBinding: ItemFooterBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(footerModel: FooterModel) {
            with(viewBinding) {
                footer.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(footerModel.footerText, Html.FROM_HTML_MODE_LEGACY)
                } else {
                    footerModel.footerText
                }
                footer.movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }
}
