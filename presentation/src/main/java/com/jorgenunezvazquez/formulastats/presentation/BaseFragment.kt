package com.jorgenunezvazquez.formulastats.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jorgenunezvazquez.formulastats.presentation.databinding.HomeBaseFragmentBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.hide
import com.jorgenunezvazquez.formulastats.presentation.utils.visible
import java.io.Serializable

private const val STATE_ARG = "bundle_arg"

abstract class BaseFragment<T : BasePresenter> : Fragment(), HomeBaseViewContract {

    protected abstract val presenter: T
    private var baseBinding: HomeBaseFragmentBinding? = null
    protected val binding: HomeBaseFragmentBinding get() = baseBinding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        baseBinding = HomeBaseFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.let { bundle ->
            (bundle.get(STATE_ARG) as? Serializable)?.let {
                presenter.onSavedInstanceState(it)
            }
        } ?: presenter.onViewCreated()
        binding.homeRecyclerView.setHasFixedSize(true)
    }

    override fun onStart() {
        super.onStart()
        if (isHidden.not()) {
            presenter.onStart()
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            presenter.onStop()
        } else {
            presenter.onStart()
        }
    }

    override fun onDestroyView() {
        binding.homeRecyclerView.adapter = null
        baseBinding = null
        super.onDestroyView()
    }

    override fun showError() {
        binding.errorLayout.visible()
    }

    override fun hideError() {
        binding.errorLayout.hide()
    }

    override fun showLoader() {
        binding.homeLoader.visible()
    }

    override fun hideLoader() {
        binding.homeLoader.hide()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        presenter.onSaveInstanceState()?.let { data ->
            outState.putSerializable(STATE_ARG, data)
        }
        super.onSaveInstanceState(outState)
    }
}

interface HomeBaseViewContract {
    fun showError()
    fun hideError()
    fun showLoader()
    fun hideLoader()
}