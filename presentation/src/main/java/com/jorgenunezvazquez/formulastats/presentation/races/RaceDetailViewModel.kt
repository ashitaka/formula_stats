package com.jorgenunezvazquez.formulastats.presentation.races

import android.content.Context
import android.graphics.drawable.Drawable
import com.jorgenunezvazquez.formulastats.domain.DateUtils.getRaceOriginalDate
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.presentation.ConstructorMarkerUtils
import com.jorgenunezvazquez.formulastats.presentation.ConstructorMarkerUtils.getConstructorMarker
import com.jorgenunezvazquez.formulastats.presentation.R
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.text.SimpleDateFormat
import java.util.*

sealed class AdapterItemModel {
    abstract fun getItemViewType(): Int
    abstract fun getItemViewId(): Long
}

data class GPInfoModel(
    val raceName: String,
    val raceImage: String,
    val flagImage: String,
    val circuitName: String,
    val locality: String,
    val raceDate: String,
    val raceTime: String
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceDetailItemViewType.GP_INFO_ITEM.viewType
    override fun getItemViewId(): Long = RaceDetailItemViewId.GP_INFO_ITEM_ID.viewId
}

data class WinnerInfoModel(
    val raceSeason: String,
    val winnerImage: String,
    val winnerName: String,
    val winnerTeamMarker: Drawable?,
    val poleDriver: String,
    val poleTeamMarker: Drawable?,
    val fastestLapDriver: String,
    val fastestLapTeamMarker: Drawable?,
    val totalLaps: String,
    val lapTime: String
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceDetailItemViewType.WINNER_INFO_ITEM.viewType
    override fun getItemViewId(): Long = RaceDetailItemViewId.WINNER_INFO_ITEM_ID.viewId
}

data class DriverResultModel(
    val driverPosition: String,
    val driverName: String,
    val driverTeam: String,
    val driverTeamMarker: Drawable?,
    val driverPoints: String,
    val driverTime: String,
    val showHeader: Boolean,
    val driverId: String,
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceDetailItemViewType.DRIVER_RESULT_ITEM.viewType
    override fun getItemViewId(): Long = driverId.hashCode().toLong()
}

data class NoResultsModel(
    val screenMessage: String
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceDetailItemViewType.NO_RESULTS_ITEM.viewType
    override fun getItemViewId(): Long = RaceDetailItemViewId.NO_RESULTS_ITEM_ID.viewId
}

enum class RaceDetailItemViewType(val viewType: Int) {
    GP_INFO_ITEM(1),
    WINNER_INFO_ITEM(-2),
    NO_RESULTS_ITEM(-3),
    DRIVER_RESULT_ITEM(-4)
}

enum class RaceDetailItemViewId(val viewId: Long) {
    GP_INFO_ITEM_ID(-1),
    WINNER_INFO_ITEM_ID(-2),
    NO_RESULTS_ITEM_ID(-3)
}

object RaceDetailModelHelper : KoinComponent {

    fun createRaceDetailItemList(
        race: Race,
        driverResultList: List<DriverResult> = emptyList(),
        winnerImageUrl: String? = null,
        raceHasFinished: Boolean = false
    ): MutableList<AdapterItemModel> {
        return mutableListOf<AdapterItemModel>().apply {
            add(getGPInfoModel(race))
            if (driverResultList.isNotEmpty()) {
                add(getWinnerInfoModel(race.season, driverResultList, winnerImageUrl.orEmpty()))
                driverResultList.forEachIndexed { index, driverResult ->
                    add(getDriverResultInfo(driverResult, isFirstItem = index == 0))
                }
            } else if (raceHasFinished.not()) {
                add(createNoResultsModel())
            }
        }
    }

    fun getGPInfoModel(race: Race): GPInfoModel = GPInfoModel(
        raceName = race.raceName,
        raceImage = race.imageUrl.orEmpty(),
        flagImage = race.flagUrl.orEmpty(),
        circuitName = race.circuit.circuitName,
        locality = race.circuit.location.locality,
        raceDate = getRaceDate(race),
        raceTime = getRaceTime(race)
    )

    private fun createNoResultsModel(): AdapterItemModel {
        val context: Context by inject()
        return NoResultsModel(
            screenMessage = context.getString(R.string.race_results_information_available_after_the_race)
        )
    }

    private fun getWinnerInfoModel(
        season: String,
        driverResultList: List<DriverResult>,
        winnerImage: String
    ): WinnerInfoModel {
        with(driverResultList) {
            val context: Context by inject()
            val winner = getWinner(this)
            val poleMan = getPoleMan(this)
            val fastestLap = getFastestLap(this)
            return WinnerInfoModel(
                raceSeason = context.getString(R.string.season_title).format(season),
                winnerImage = winnerImage,
                winnerName = winner?.driver?.familyName.orEmpty(),
                winnerTeamMarker = getConstructorMarker(winner?.constructor?.constructorId.orEmpty()),
                poleDriver = poleMan?.driver?.familyName.orEmpty(),
                poleTeamMarker = getConstructorMarker(poleMan?.constructor?.constructorId.orEmpty()),
                fastestLapDriver = fastestLap?.driver?.familyName.orEmpty(),
                fastestLapTeamMarker = getConstructorMarker(fastestLap?.constructor?.constructorId.orEmpty()),
                totalLaps = getLaps(this).orEmpty(),
                lapTime = getTime(this).orEmpty()
            )
        }
    }

    private fun getDriverResultInfo(
        driverResult: DriverResult,
        isFirstItem: Boolean
    ): DriverResultModel {
        with(driverResult) {
            return DriverResultModel(
                driverPosition = position,
                driverName = driver.familyName,
                driverTeam = constructor.name,
                driverTeamMarker = getConstructorMarker(constructor.constructorId),
                driverPoints = points,
                driverTime = time?.time ?: status,
                showHeader = isFirstItem,
                driverId = driverResult.driver.driverId
            )
        }
    }

    private fun getWinner(driverResultList: List<DriverResult>?): DriverResult? {
        return driverResultList?.find { it.position == "1" }
    }

    private fun getTime(driverResultList: List<DriverResult>?): String? {
        return driverResultList?.find { it.position == "1" }?.time?.time
    }

    private fun getPoleMan(driverResultList: List<DriverResult>?): DriverResult? {
        return driverResultList?.find { it.grid == "1" }
    }

    private fun getLaps(driverResultList: List<DriverResult>?): String? {
        return driverResultList?.find { it.position == "1" }?.laps
    }

    private fun getFastestLap(driverResultList: List<DriverResult>?): DriverResult? {
        return driverResultList?.find { it.fastestLap?.rank == "1" }
    }

    private fun getRaceDate(race: Race): String {
        val raceDate = getRaceOriginalDate(race)
        val newDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return raceDate?.let { newDateFormat.format(raceDate) }.orEmpty()
    }

    private fun getRaceTime(race: Race): String {
        if (race.time == null) return "-"
        val raceDate = getRaceOriginalDate(race)
        val newTimeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        return raceDate?.let { newTimeFormat.format(raceDate) }.orEmpty()
    }
}
