package com.jorgenunezvazquez.formulastats.presentation.drivers

import android.content.Context
import android.graphics.drawable.Drawable
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.presentation.ConstructorMarkerUtils
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailsModelHelper.getDriverName
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.Serializable

data class DriverStandingsModel(
    val driverPosition: String,
    val driverPoints: String,
    val driverName: String,
    val driverId: String,
    val driverConstructor: String,
    val constructorMarker: Drawable?,
    val driverImage: String,
    val flagImage: String
) : Serializable

object DriverStandingsModelHelper : KoinComponent {

    fun getDriverListModel(driverStandings: List<DriverStandings>): List<DriverStandingsModel> {
        return mutableListOf<DriverStandingsModel>().apply {
            driverStandings.forEach { driverStanding ->
                add(getDriverStandingModel(driverStanding))
            }
        }
    }

    private fun getDriverStandingModel(driverStanding: DriverStandings): DriverStandingsModel {
        val context: Context by inject()
        with(driverStanding) {
            return DriverStandingsModel(
                driverPosition = position,
                driverPoints = String.format(context.resources.getString(R.string.points_sufix), points),
                driverName = getDriverName(driver),
                driverId = driver.driverId,
                driverConstructor = constructors.firstOrNull()?.name.orEmpty(), //TODO Review design for case with two or more constructors
                constructorMarker = constructors.firstOrNull()?.let { ConstructorMarkerUtils.getConstructorMarker(it.constructorId) },
                driverImage = driver.imageUrl,
                flagImage = driver.flagUrl
            )
        }
    }
}