package com.jorgenunezvazquez.formulastats.presentation


enum class ConstructorStyle(val id: String, val style: Int, val color: Int, val logo: Int, val logoBackgroundColor: Int = color) {
    MERCEDES("mercedes", R.style.Mercedes, R.color.mercedes, R.drawable.ic_mercedes_amg_petronas_f1_logo),
    FERRARI("ferrari", R.style.Ferrari, R.color.ferrari, R.drawable.ic_scuderia_ferrari_logo),
    RED_BULL("red_bull", R.style.RedBull, R.color.red_bull, R.drawable.oraclerbr, R.color.red_bull_background),
    ALPINE("alpine", R.style.Alpine, R.color.alpine, R.drawable.ic_alpine_f1_team_logo),
    MCLAREN("mclaren", R.style.McLaren, R.color.mclaren, R.drawable.ic_mclaren_racing_logo),
    HAAS("haas", R.style.Haas, R.color.haas, R.drawable.ic_haas_f1_team_logo),
    ALFA("alfa", R.style.Alfa, R.color.alfa, R.drawable.ic_logo_alfa_romeo_racing_orlen),
    ASTON_MARTIN("aston_martin", R.style.AstonMartin, R.color.aston_martin, R.drawable.ic_aston_martin_f1),
    ALPHA_TAURI("alphatauri", R.style.AlphaTauri, R.color.alpha_tauri, R.drawable.ic_scuderia_alpha_tauri),
    SAUBER("sauber", R.style.Sauber, R.color.sauber, R.drawable.ic_stake_f1_team),
    VISA_CASH_RB("rb", R.style.RBF1Team, R.color.rb_f1_team, R.drawable.ic_visa_cash_logo),
    WILLIAMS("williams", R.style.Williams, R.color.williams, R.drawable.ic_williams_racing_1),
    DEFAULT("default", R.style.Default, R.color.colorPrimary, R.drawable.ic_constructors);

    companion object {
        fun getValueById(id: String): ConstructorStyle {
            for (team: ConstructorStyle in values()) {
                if (id == team.id) {
                    return team
                }
            }
            return DEFAULT
        }

        fun getConstructorLogoById(constructorId: String) = values().firstOrNull { it.id == constructorId }?.logo ?: DEFAULT.logo

        fun getConstructorLogoBackgroundColorById(constructorId: String) = values().firstOrNull { it.id == constructorId }?.logoBackgroundColor ?: DEFAULT.color
    }
}