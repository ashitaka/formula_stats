package com.jorgenunezvazquez.formulastats.presentation.constructors

import android.os.Bundle
import android.view.Gravity
import android.widget.ArrayAdapter
import androidx.navigation.ActivityNavigator
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.jorgenunezvazquez.formulastats.presentation.BaseActivity
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.SeasonSpinnerAdapterListener
import com.jorgenunezvazquez.formulastats.presentation.databinding.ActivityConstructorDetailBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemConstructorDriverStatsBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.*
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class ConstructorDetailActivity : BaseActivity<ConstructorDetailPresenter>(),
    ConstructorDetailViewContract {

    override val presenter: ConstructorDetailPresenter by inject { parametersOf(this) }
    private lateinit var binding: ActivityConstructorDetailBinding

    companion object {
        private const val ACTIVITY_NAME = "ConstructorDetail"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConstructorDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.myToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        FirebaseUtils.sendViewSelectedEvent(ACTIVITY_NAME, this)
    }

    override fun showConstructorData(constructorDataModel: ConstructorDataModel) {
        with(binding) {
            supportActionBar?.title = constructorDataModel.constructorName
            if (constructorDataModel.flagImageUrl.isNotEmpty()) {
                Picasso.get().load(constructorDataModel.flagImageUrl).fit().centerCrop()
                    .into(constructorFlag)
            }
            constructorLogo.setImageResource(constructorDataModel.constructorLogo)
            constructorNationality.text = constructorDataModel.constructorNationality
        }
    }

    override fun showConstructorDriversData(constructorDriverDataModelList: List<ConstructorDriverDataModel>) {
        binding.driverStandingsContainer.removeAllViews()
        constructorDriverDataModelList.forEach { constructorDriverDataModel ->
            with(constructorDriverDataModel) {
                val view = ItemConstructorDriverStatsBinding.inflate(layoutInflater).apply {
                    driverCode.text = constructorDriverCode
                    driverName.text = constructorDriverName
                    driverPoints.text = constructorDriverPoints
                    driverPosition.text = constructorDriverPosition
                    if (constructorDriverImageUrl.isNotBlank()) {
                        Picasso.get().load(constructorDriverImageUrl).fit().noFade()
                            .centerCrop(Gravity.TOP).into(driverImageItem)
                    } else {
                        driverImageItem.hide()
                    }
                }
                binding.driverStandingsContainer.addView(view.root)
            }
        }
    }

    override fun showConstructorSeasonStats(constructorSeasonStatsModel: ConstructorSeasonStatsModel) {
        with(constructorSeasonStatsModel) {
            binding.seasonDetailsTitle.text = seasonTitle
            binding.position.text = seasonPosition
            binding.currentRound.text = seasonRound
            binding.wins.text = seasonWins
            binding.points.text = seasonPoints
        }
    }

    override fun showConstructorSeasonCharts(constructorSeasonChartsModel: ConstructorSeasonChartsModel) {
        with(constructorSeasonChartsModel) {
            binding.constructorGridChartTitle.text = gridChartTitle
            binding.resultsTitle.text = resultsChartTitle
            showResultsChart(this)
            showGridChart(this)
            binding.constructorsChartGroup.visible()
        }
    }

    override fun showConstructorTotalStats(constructorTotalStatsModel: ConstructorTotalStatsModel) {
        with(constructorTotalStatsModel) {
            binding.detailTitles.text = titles
            binding.allWins.text = totalWins
            binding.seasons.text = totalSeasons
            binding.allPoints.text = totalPoints
            setSeasonsSpinner(seasonList)
        }
    }

    private fun getChartMap(rounds: List<Float>, values: List<Float>): List<Entry> {
        return rounds.mapIndexed { index, round ->
            Entry(round, values[index])
        }
    }

    private fun showResultsChart(constructorSeasonChartsModel: ConstructorSeasonChartsModel) {
        with(constructorSeasonChartsModel) {
            val driversDataSetList = mutableListOf<LineDataSet>()
            constructorSeasonDriverChartModel.forEach { constructorSeasonDriverChartModel ->
                driversDataSetList.add(
                    getLineDataSet(
                        constructorSeasonDriverChartModel,
                        constructorSeasonDriverChartModel.driverSeasonPositions
                    )
                )
            }
            ChartUtils.setChart(driversDataSetList, binding.driversResultsChart, true)
        }
    }

    private fun showGridChart(constructorSeasonChartsModel: ConstructorSeasonChartsModel) {
        with(constructorSeasonChartsModel) {
            val driversDataSetList = mutableListOf<LineDataSet>()
            constructorSeasonDriverChartModel.forEach { driverChartModel ->
                val chartMap = getChartMap(
                    driverChartModel.driverRounds,
                    driverChartModel.driverSeasonGrids
                )
                val nonZeroChartMap = chartMap.filter { it.y != 0f }
                driversDataSetList.add(LineDataSet(nonZeroChartMap, driverChartModel.driverCode))
            }
            ChartUtils.setChart(driversDataSetList, binding.driversGridChart, true)
        }
    }

    private fun getLineDataSet(
        driverChartModel: ConstructorSeasonDriverChartModel,
        values: List<Float>
    ): LineDataSet {
        return LineDataSet(
            getChartMap(driverChartModel.driverRounds, values),
            driverChartModel.driverCode
        )
    }

    private fun setSeasonsSpinner(seasons: List<String>) {
        binding.seasonsSpinner.apply {
            adapter = ArrayAdapter(
                this@ConstructorDetailActivity,
                android.R.layout.simple_spinner_dropdown_item,
                seasons
            )
            onItemSelectedListener = SeasonSpinnerAdapterListener { season ->
                presenter.getSeasonDriverResults(season)
            }
            setSelection(seasons.lastIndex, false)
            visible()
        }
    }

    override fun showProgressBar() {
        binding.detailProgressBar.visible()
    }

    override fun hideProgressBar() {
        binding.detailProgressBar.hide()
    }

    override fun showError() {
        hideProgressBar()
        this.showToast(R.string.error_retrieving_data)
    }

    override fun getConstructorDetailParamsFromArgs(): ConstructorDetailParams? {
        return intent?.extras?.let { ConstructorDetailActivityArgs.fromBundle(it) }?.constructorDetailParams
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}
