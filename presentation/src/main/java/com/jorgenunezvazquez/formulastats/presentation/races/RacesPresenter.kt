package com.jorgenunezvazquez.formulastats.presentation.races

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.useCases.main.LoadCountriesUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.races.GetCurrentRaceScheduleUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.HomeBaseViewContract
import com.jorgenunezvazquez.formulastats.presentation.races.RacesModelHelper.getRaceScheduleModel
import org.koin.core.component.KoinComponent

class RacesPresenter(
    private val view: RacesViewContract,
    private val getCurrentRaceScheduleUseCase: GetCurrentRaceScheduleUseCase,
    invoker: Invoker
) : BasePresenter(invoker), KoinComponent {

    private var races: List<Race> = emptyList()

    override fun onViewCreated() {
        super.onViewCreated()
        if (races.isEmpty()) {
            getCurrentRaceSchedule()
        } else {
            view.hideLoader()
            view.showRaces(getRaceScheduleModel(races))
        }
    }

    private fun getCurrentRaceSchedule() {
        view.showLoader()
        invoker.execute(getCurrentRaceScheduleUseCase) { result ->
            if (result is Success) {
                races = result.data
                view.showRaces(getRaceScheduleModel(result.data))
            } else {
                view.showError()
            }
            view.hideLoader()
        }
    }

    fun onRaceClicked(raceName: String) {
        val raceSelected = races.find { it.raceName == raceName }
        raceSelected?.let {
            view.goToRaceDetails(it)
        }
    }
}

interface RacesViewContract : HomeBaseViewContract {
    fun showRaces(races: List<AdapterItemModel>)
    fun goToRaceDetails(race: Race)
}