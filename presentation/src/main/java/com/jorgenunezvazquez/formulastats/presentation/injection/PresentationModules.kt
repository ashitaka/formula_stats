package com.jorgenunezvazquez.formulastats.presentation.injection

import android.app.Activity
import androidx.fragment.app.Fragment
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailActivity
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailPresenter
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorStandingsFragment
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorStandingsPresenter
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailActivity
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailPresenter
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverStandingsFragment
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverStandingsPresenter
import com.jorgenunezvazquez.formulastats.presentation.main.MainActivity
import com.jorgenunezvazquez.formulastats.presentation.main.MainPresenter
import com.jorgenunezvazquez.formulastats.presentation.races.RaceDetailActivity
import com.jorgenunezvazquez.formulastats.presentation.races.RaceDetailPresenter
import com.jorgenunezvazquez.formulastats.presentation.races.RacesFragment
import com.jorgenunezvazquez.formulastats.presentation.races.RacesPresenter
import org.koin.dsl.module


object PresentationModules {

    val presentationModule = module {

        factory {  (view: Activity) ->
            MainPresenter(
                view as MainActivity,
                get(),
                get()
            )
        }

        factory { (fragment: Fragment) ->
            RacesPresenter(
                fragment as RacesFragment,
                get(),
                get()
            )
        }

        factory { (view: Activity) ->
            RaceDetailPresenter(
                view as RaceDetailActivity,
                get(),
                get()
            )
        }

        factory { (fragment: Fragment) ->
            DriverStandingsPresenter(
                fragment as DriverStandingsFragment,
                get(),
                get()
            )
        }

        factory { (view: Activity) ->
            DriverDetailPresenter(
                view as DriverDetailActivity,
                get(),
                get(),
                get()
            )
        }

        factory { (fragment: Fragment) ->
            ConstructorStandingsPresenter(
                fragment as ConstructorStandingsFragment,
                get(),
                get()
            )
        }

        factory { (view: Activity) ->
            ConstructorDetailPresenter(
                view as ConstructorDetailActivity,
                get(),
                get(),
                get()
            )
        }
    }
}
