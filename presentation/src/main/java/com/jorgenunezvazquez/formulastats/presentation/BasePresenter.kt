package com.jorgenunezvazquez.formulastats.presentation

import com.jorgenunezvazquez.formulastats.domain.Invoker
import java.io.Serializable

open class BasePresenter(val invoker: Invoker) {

    open fun onPostCreate() {}

    open fun onViewCreated() {}

    open fun onSaveInstanceState(): Serializable? = null

    open fun onSavedInstanceState(data: Serializable) {}

    open fun onStart() {}

    open fun onStop() {
        invoker.cancelTasks()
    }
}