package com.jorgenunezvazquez.formulastats.presentation.races

import androidx.navigation.fragment.findNavController
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.presentation.BaseFragment
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class RacesFragment : BaseFragment<RacesPresenter>(), RacesViewContract {

    override val presenter: RacesPresenter by inject { parametersOf(this) }

    override fun showRaces(races: List<AdapterItemModel>) {
        val racesAdapter = RacesAdapter(races) { race ->
            presenter.onRaceClicked(race)
        }
        binding.homeRecyclerView.adapter = racesAdapter
    }

    override fun goToRaceDetails(race: Race) {
        val directions = RacesFragmentDirections.actionRacesFragmentToRaceDetailActivity(race)
        findNavController().navigate(directions)
    }
}


