package com.jorgenunezvazquez.formulastats.presentation.races

import android.content.Context
import com.jorgenunezvazquez.formulastats.domain.DateUtils.getRaceOriginalDate
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.races.RaceDetailModelHelper.getGPInfoModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.text.SimpleDateFormat
import java.util.*

data class RaceModel(
    val raceName: String,
    val raceCircuit: String,
    val raceOrder: String,
    val raceLocation: String,
    val raceDate: String?,
    val raceFlagUrl: String
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceItemViewType.GP_SCHEDULE_INFO_ITEM.viewType
    override fun getItemViewId(): Long = raceName.hashCode().toLong()
}

data class FooterModel(
    val footerText: String
) : AdapterItemModel() {
    override fun getItemViewType(): Int = RaceItemViewType.FOOTER_ITEM.viewType
    override fun getItemViewId(): Long = RaceItemViewId.FOOTER_ITEM.viewId
}

object RacesModelHelper : KoinComponent {

    fun getRaceScheduleModel(raceList: List<Race>): List<AdapterItemModel> {
        val context: Context by inject()
        return mutableListOf<AdapterItemModel>().apply {
            getNextRaceGpInfo(raceList)?.let { add(it) }
            raceList.forEach { race ->
                add(
                    RaceModel(
                        race.raceName,
                        race.circuit.circuitName,
                        race.round,
                        String.format("%s - %s", race.circuit.location.locality, race.circuit.location.country),
                        getRaceDate(race),
                        race.flagUrl.orEmpty()
                    )
                )
            }
            add(FooterModel(context.resources.getString(R.string.powered_by_ergast_api)))
        }
    }

    private fun getRaceDate(race: Race): String? {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val newDateFormat = SimpleDateFormat("dd-MM-yy", Locale.getDefault())
        val raceDateString = format.parse(race.date)
        return raceDateString?.let { newDateFormat.format(it) }
        //TODO REVIEW PARSING EXCEPTIONS
    }

    private fun getNextRaceGpInfo(raceList: List<Race>): GPInfoModel? {
        val nextRace = getNextRace(raceList)
        return nextRace?.let {
            getGPInfoModel(nextRace)
        }
    }

    private fun getNextRace(raceList: List<Race>): Race? = try {
        val currentDate = Calendar.getInstance().time
        raceList.firstOrNull {
            val raceDate = getRaceOriginalDate(it)
            if (raceDate != null) {
                currentDate < raceDate
            } else {
                false
            }
        }
    } catch (e: Exception) {
        null
    }
}

enum class RaceItemViewType(val viewType: Int) {
    GP_SCHEDULE_INFO_ITEM(2),
    FOOTER_ITEM(3)
}

enum class RaceItemViewId(val viewId: Long) {
    FOOTER_ITEM(-2)
}