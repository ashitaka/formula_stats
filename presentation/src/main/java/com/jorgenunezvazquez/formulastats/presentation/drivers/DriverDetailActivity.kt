package com.jorgenunezvazquez.formulastats.presentation.drivers

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.widget.ArrayAdapter
import androidx.navigation.ActivityNavigator
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.jorgenunezvazquez.formulastats.presentation.BaseActivity
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.SeasonSpinnerAdapterListener
import com.jorgenunezvazquez.formulastats.presentation.databinding.ActivityDriverDetailBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.*
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class DriverDetailActivity : BaseActivity<DriverDetailPresenter>(), DriverDetailViewContract {

    override val presenter: DriverDetailPresenter by inject { parametersOf(this) }
    private lateinit var binding: ActivityDriverDetailBinding

    companion object {
        private const val ACTIVITY_NAME = "DriverDetail"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDriverDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.myToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        FirebaseUtils.sendViewSelectedEvent(ACTIVITY_NAME, this)
    }

    override fun showDriverData(driverDataModel: DriverDataModel) {
        with(driverDataModel) {
            supportActionBar?.title = driverName
            showDriverImage(driverImageUrl)
            if (flagImageUrl.isNotEmpty()) {
                Picasso.get().load(flagImageUrl).fit().centerCrop().into(binding.driverFlag)
            }
            binding.driverDetailsNationality.text = driverNationality
            binding.number.text = driverNumber
            binding.code.text = driverCode
            binding.age.text = driverAge
        }
    }

    override fun showDriverSeasonStats(driverSeasonStatsModel: DriverSeasonStatsModel) {
        with(driverSeasonStatsModel) {
            binding.seasonDetailsTitle.text = seasonTitle
            binding.position.text = seasonPosition
            binding.team.text = seasonTeam
            binding.wins.text = seasonWins
            binding.points.text = seasonPoints
        }
    }

    override fun showDriverSeasonCharts(driverSeasonChartsModel: DriverSeasonChartsModel) {
        with(driverSeasonChartsModel) {
            binding.driverDetailCharts.visible()
            binding.pointsChartDetailTitle.text = pointsChartTitle
            binding.resultsTitle.text = resultsChartTitle
            showResultsChart(driverSeasonChartsModel)
            showPointsChart(driverSeasonChartsModel)
        }
    }

    override fun showDriverTotalStats(driverTotalStatsModel: DriverTotalStatsModel) {
        with(driverTotalStatsModel) {
            binding.allWins.text = totalWins
            binding.podiums.text = totalPodiums
            binding.poles.text = totalPoles
            binding.fLap.text = totalFastestLap
            binding.bestPos.text = bestPosition
            binding.seasons.text = totalSeasons
            binding.allPoints.text = totalPoints
            binding.driverTitles.text = titles
            setSeasonsSpinner(seasonList)
        }
    }

    private fun showDriverImage(imageUrl: String) {
        if (imageUrl.isNotEmpty()) {
            Picasso.get().load(imageUrl).fit().noFade().centerCrop(Gravity.TOP)
                .into(binding.driverImageItem)
        }
    }

    private fun showResultsChart(driverSeasonChartsModel: DriverSeasonChartsModel) {
        val resultEntries =
            getChartMap(driverSeasonChartsModel.rounds, driverSeasonChartsModel.seasonPositions)
        val gridEntries = getChartMap(
            driverSeasonChartsModel.seasonGridRounds,
            driverSeasonChartsModel.seasonGrids
        )
        val resultsDataSet = LineDataSet(resultEntries, resources.getString(R.string.final_position))
        val gridDataSet = LineDataSet(gridEntries, resources.getString(R.string.grid))

        resultsDataSet.apply {
            color = Color.GREEN
            setDrawValues(false)
        }

        gridDataSet.apply {
            color = Color.MAGENTA
            setDrawValues(false)
        }

        binding.resultsChart.data = LineData(resultsDataSet, gridDataSet)
        ChartUtils.styleChart(true, binding.resultsChart)
    }

    private fun showPointsChart(driverSeasonChartsModel: DriverSeasonChartsModel) {
        var accumulated = 0F
        val pointsAccumulated = driverSeasonChartsModel.seasonPoints.map { racePoints ->
                val newValue = racePoints + accumulated
                accumulated = newValue
                newValue
            }
        val pointEntries = getChartMap(driverSeasonChartsModel.rounds, pointsAccumulated)
        val resultsDataSet = LineDataSet(pointEntries, resources.getString(R.string.points))

        resultsDataSet.apply {
            color = Color.CYAN
            setDrawValues(false)
        }

        binding.pointsChart.data = LineData(resultsDataSet)
        ChartUtils.styleChart(false, binding.pointsChart)
    }

    private fun getChartMap(rounds: List<Float>, values: List<Float>): List<Entry> {
        return rounds.mapIndexed { index, round -> Entry(round, values[index]) }
    }

    private fun setSeasonsSpinner(seasons: List<String>) {
        binding.seasonsSpinner.apply {
            adapter = ArrayAdapter(
                this@DriverDetailActivity,
                android.R.layout.simple_spinner_dropdown_item,
                seasons
            )
            onItemSelectedListener = SeasonSpinnerAdapterListener { season ->
                presenter.getDriverStandingsBySeason(season)
            }
            setSelection(seasons.lastIndex)
            visible()
        }
    }

    override fun showProgressBar() {
        binding.detailProgressBar.visible()
    }

    override fun hideProgressBar() {
        binding.detailProgressBar.hide()
    }

    override fun showError() {
        showToast(R.string.error_retrieving_data)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun getDriverDetailParamsFromArgs(): DriverDetailParams? {
        return intent?.extras?.let { DriverDetailActivityArgs.fromBundle(it) }?.driverDetailParams
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}
