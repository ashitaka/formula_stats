package com.jorgenunezvazquez.formulastats.presentation.constructors

import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import java.io.Serializable

data class ConstructorDetailParams(
    val constructorStandings: ConstructorStandings,
    val round: String,
    val season: String
): Serializable