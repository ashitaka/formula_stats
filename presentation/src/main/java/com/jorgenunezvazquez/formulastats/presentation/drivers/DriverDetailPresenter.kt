package com.jorgenunezvazquez.formulastats.presentation.drivers

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.DriverStandingsParams
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetAllDriverResultsByIdUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriverStandingsBySeasonUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailsModelHelper.getDriverDataModel
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailsModelHelper.getDriverSeasonChartsModel
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailsModelHelper.getDriverSeasonStatsModel
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverDetailsModelHelper.getDriverTotalStatsModel

class DriverDetailPresenter(
    private val view: DriverDetailViewContract,
    private val getAllDriverResultsByIdUseCase: GetAllDriverResultsByIdUseCase,
    private val getDriverStandingsBySeasonUseCase: GetDriverStandingsBySeasonUseCase,
    invoker: Invoker
) : BasePresenter(invoker) {

    private var driverRaceResultList: List<DriverRaceResult> = mutableListOf()

    override fun onPostCreate() {
        super.onPostCreate()
        view.getDriverDetailParamsFromArgs()?.let {
            view.showDriverData(getDriverDataModel(it.driverStandings))
            view.showDriverSeasonStats(getDriverSeasonStatsModel(it.driverStandings, it.season))
            getAllDriverResultsById(it.driverStandings.driver.driverId)
        }
    }

    fun getDriverStandingsBySeason(season: String) {
        view.getDriverDetailParamsFromArgs()?.driverStandings?.driver?.driverId?.let { driverId ->
            view.showProgressBar()
            invoker.cancelTasks()
            invoker.execute(getDriverStandingsBySeasonUseCase withParams DriverStandingsParams(season, driverId)) {
                if (it is Success) {
                    getDriverSeasonChartsModel(getDriverResultsBySeason(season), it.data.round)?.let { seasonChartsModel -> view.showDriverSeasonCharts(seasonChartsModel) }
                    it.data.driverStandings.firstOrNull()?.let { driverStandings ->
                        view.showDriverSeasonStats(getDriverSeasonStatsModel(driverStandings, season))
                    } ?: view.showError()
                } else {
                    view.showError()
                }
                view.hideProgressBar()
            }
        } ?: view.showError()
    }

    private fun getAllDriverResultsById(driverId: String) {
        view.showProgressBar()
        invoker.execute(getAllDriverResultsByIdUseCase withParams driverId) {
            if (it is Success) {
                driverRaceResultList = it.data.results
                view.showDriverTotalStats(getDriverTotalStatsModel(it.data))
            } else {
                view.showError()
            }
            view.hideProgressBar()
        }
    }

    private fun getDriverResultsBySeason(season: String): MutableList<DriverRaceResult> {
        return driverRaceResultList.filter { it.season == season }.toMutableList()
    }
}

interface DriverDetailViewContract {
    fun getDriverDetailParamsFromArgs(): DriverDetailParams?
    fun showDriverData(driverDataModel: DriverDataModel)
    fun showDriverSeasonStats(driverSeasonStatsModel: DriverSeasonStatsModel)
    fun showDriverSeasonCharts(driverSeasonChartsModel: DriverSeasonChartsModel)
    fun showDriverTotalStats(driverTotalStatsModel: DriverTotalStatsModel)
    fun showError()
    fun showProgressBar()
    fun hideProgressBar()
}