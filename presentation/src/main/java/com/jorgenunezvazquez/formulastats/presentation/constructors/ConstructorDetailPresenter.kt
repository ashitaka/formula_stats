package com.jorgenunezvazquez.formulastats.presentation.constructors

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetAllConstructorStandingsByIdUseCase
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetConstructorDriverResultsParams
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetConstructorDriversResultsBySeasonUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailsModelHelper.getConstructorDataModel
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailsModelHelper.getConstructorDriversDataModel
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailsModelHelper.getConstructorSeasonChartsModel
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailsModelHelper.getConstructorSeasonStatsModel
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorDetailsModelHelper.getConstructorTotalStatsModel


class ConstructorDetailPresenter(
    private val view: ConstructorDetailViewContract,
    private val getAllConstructorStandingsByIdUseCase: GetAllConstructorStandingsByIdUseCase,
    private val getConstructorDriversResultsBySeasonUseCase: GetConstructorDriversResultsBySeasonUseCase,
    invoker: Invoker
) : BasePresenter(invoker) {

    override fun onPostCreate() {
        super.onPostCreate()
        view.getConstructorDetailParamsFromArgs()?.let {
            view.showConstructorData(getConstructorDataModel(it.constructorStandings))
            view.showConstructorSeasonStats(getConstructorSeasonStatsModel(it.constructorStandings, it.season, it.round))
            view.showConstructorDriversData(getConstructorDriversDataModel(it.constructorStandings.driverStandings))
            getAllConstructorStandingsById(it.constructorStandings.constructor.constructorId)
        }
    }

    fun getSeasonDriverResults(season: String) {
        view.getConstructorDetailParamsFromArgs()?.constructorStandings?.constructor?.constructorId?.let { constructorId ->
            view.showProgressBar()
            invoker.cancelTasks()
            invoker.execute(getConstructorDriversResultsBySeasonUseCase withParams GetConstructorDriverResultsParams(season, constructorId)) { result ->
                if (result is Success) {
                    view.showConstructorSeasonStats(getConstructorSeasonStatsModel(result.data.constructorSeasonStandings, season, result.data.round))
                    view.showConstructorDriversData(getConstructorDriversDataModel(result.data.driverStandings))
                    getConstructorSeasonChartsModel(result.data.driverSeasonResult, season)?.let {
                        view.showConstructorSeasonCharts(it)
                    } ?: view.showError()
                } else {
                    view.showError()
                }
                view.hideProgressBar()
            }
        }
    }

    private fun getAllConstructorStandingsById(constructorId: String) {
        view.getConstructorDetailParamsFromArgs()?.let { params ->
            view.showProgressBar()
            invoker.execute(getAllConstructorStandingsByIdUseCase withParams constructorId) {
                if (it is Success) {
                    val constructorStandingsWrapperList = it.data.toMutableList().apply {
                        if (none { constructorStanding -> constructorStanding.season == params.season }) {
                            add(ConstructorStandingsWrapper(params.season, params.round, mutableListOf(params.constructorStandings)))
                        }
                    }
                    view.showConstructorTotalStats(getConstructorTotalStatsModel(constructorStandingsWrapperList))
                } else {
                    view.showError()
                }
            }
        }
    }
}

interface ConstructorDetailViewContract {
    fun getConstructorDetailParamsFromArgs(): ConstructorDetailParams?
    fun showConstructorData(constructorDataModel: ConstructorDataModel)
    fun showConstructorDriversData(constructorDriverDataModelList: List<ConstructorDriverDataModel>)
    fun showConstructorSeasonStats(constructorSeasonStatsModel: ConstructorSeasonStatsModel)
    fun showConstructorSeasonCharts(constructorSeasonChartsModel: ConstructorSeasonChartsModel)
    fun showConstructorTotalStats(constructorTotalStatsModel: ConstructorTotalStatsModel)
    fun showError()
    fun showProgressBar()
    fun hideProgressBar()
}