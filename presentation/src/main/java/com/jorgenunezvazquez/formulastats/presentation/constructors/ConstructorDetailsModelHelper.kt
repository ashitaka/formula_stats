package com.jorgenunezvazquez.formulastats.presentation.constructors

import android.content.Context
import android.graphics.drawable.Drawable
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.presentation.ConstructorMarkerUtils
import com.jorgenunezvazquez.formulastats.presentation.ConstructorStyle
import com.jorgenunezvazquez.formulastats.presentation.R
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

private const val FIRST_POSITION_ID = "1"

data class ConstructorDataModel(
    val flagImageUrl: String,
    val constructorNationality: String,
    val constructorName: String,
    val constructorLogo: Int
)

data class ConstructorTotalStatsModel(
    val titles: String,
    val totalWins: String,
    val totalSeasons: String,
    val totalPoints: String,
    val seasonList: List<String>
)

data class ConstructorSeasonStatsModel(
    val seasonTitle: String,
    val seasonPosition: String,
    val seasonRound: String,
    val seasonTeamMarker: Drawable?,
    val seasonWins: String,
    val seasonPoints: String
)

data class ConstructorDriverDataModel(
    val constructorDriverName: String,
    val constructorDriverImageUrl: String,
    val constructorDriverPosition: String,
    val constructorDriverPoints: String,
    val constructorDriverCode: String,
    val constructorDriverWins: String
)

data class ConstructorSeasonChartsModel(
    val rounds: List<Float>,
    val gridChartTitle: String,
    val resultsChartTitle: String,
    val constructorSeasonDriverChartModel: List<ConstructorSeasonDriverChartModel>
)

data class ConstructorSeasonDriverChartModel(
    val driverCode: String,
    val driverSeasonPositions: List<Float>,
    val driverSeasonGrids: List<Float>,
    val driverRounds: List<Float>
)

object ConstructorDetailsModelHelper : KoinComponent {

    fun getConstructorDataModel(constructorStandings: ConstructorStandings): ConstructorDataModel {
        return with(constructorStandings.constructor) {
            ConstructorDataModel(
                flagImageUrl = flagUrl,
                constructorNationality = nationality,
                constructorName = constructorStandings.constructor.name,
                constructorLogo = ConstructorStyle.getConstructorLogoById(constructorStandings.constructor.constructorId)
            )
        }
    }

    fun getConstructorDriversDataModel(driverStandingsList: List<DriverStandings>): List<ConstructorDriverDataModel> {
        return mutableListOf<ConstructorDriverDataModel>().apply {
            driverStandingsList.forEach { driverStandings ->
                val driver = driverStandings.driver
                add(
                    ConstructorDriverDataModel(
                        constructorDriverCode = driver.code.ifEmpty { driver.familyName.substring(0, 3).uppercase() },
                        constructorDriverName = driver.familyName,
                        constructorDriverImageUrl = driver.imageUrl,
                        constructorDriverPosition = driverStandings.position,
                        constructorDriverPoints = driverStandings.points,
                        constructorDriverWins = driverStandings.wins
                    )
                )
            }
        }
    }

    fun getConstructorSeasonStatsModel(constructorStandings: ConstructorStandings, season: String, round: String): ConstructorSeasonStatsModel {
        val context: Context by inject()
        return with(constructorStandings) {
            ConstructorSeasonStatsModel(
                seasonTitle = String.format(context.resources.getString(R.string.season_title), season),
                seasonPosition = position,
                seasonRound = round,
                seasonTeamMarker = ConstructorMarkerUtils.getConstructorMarker(constructor.constructorId),
                seasonWins = wins,
                seasonPoints = points
            )
        }
    }

    fun getConstructorSeasonChartsModel(driversRaceResultList: List<List<DriverRaceResult>>, season: String): ConstructorSeasonChartsModel? {
        val context: Context by inject()
        return try {
            ConstructorSeasonChartsModel(
                rounds = driversRaceResultList.first().map { it.round.toFloat() },
                gridChartTitle = String.format(context.resources.getString(R.string.grid_season), season),
                resultsChartTitle = String.format(context.resources.getString(R.string.results_season), season),
                constructorSeasonDriverChartModel = driversRaceResultList.toConstructorSeasonDriverChartModelList()
            )
        } catch (e: Exception) {
            null
        }
    }

    fun getConstructorTotalStatsModel(constructorStandingsWrapperList: List<ConstructorStandingsWrapper>): ConstructorTotalStatsModel {
        with(constructorStandingsWrapperList) {
            return ConstructorTotalStatsModel(
                totalWins = getConstructorWins(this),
                totalSeasons = getConstructorSeasons(this),
                totalPoints = getConstructorPoints(this),
                titles = getConstructorTitles(this),
                seasonList = map { it.season }
            )
        }
    }

    private fun List<List<DriverRaceResult>>.toConstructorSeasonDriverChartModelList() =
        mutableListOf<ConstructorSeasonDriverChartModel>().apply {
            try {
                this@toConstructorSeasonDriverChartModelList.forEach { driverRaceResultList ->
                    driverRaceResultList.firstOrNull()?.results?.driver?.let { driver ->
                        add(ConstructorSeasonDriverChartModel(
                            driverCode = driver.code.ifEmpty { driver.familyName.substring(0, 3).uppercase() },
                            driverSeasonGrids = driverRaceResultList.map { it.results.grid.toFloat() },
                            driverSeasonPositions = driverRaceResultList.map { it.results.position.toFloat() },
                            driverRounds = driverRaceResultList.map { it.round.toFloat() }
                        ))
                    }
                }
            } catch (e: Exception) {
                listOf<ConstructorSeasonDriverChartModel>()
            }
        }

    private fun getConstructorTitles(constructorStandingsWrapperList: List<ConstructorStandingsWrapper>): String {
        return constructorStandingsWrapperList.filter { it.constructorStandings.first().position == FIRST_POSITION_ID }.count().toString()
    }

    private fun getConstructorWins(constructorStandingsWrapperList: List<ConstructorStandingsWrapper>): String {
        return constructorStandingsWrapperList.sumOf { it.constructorStandings.first().wins.toInt() }.toString()
    }

    private fun getConstructorPoints(constructorStandingsWrapperList: List<ConstructorStandingsWrapper>): String {
        return constructorStandingsWrapperList.sumOf { it.constructorStandings.first().points.toDouble() }.toString()
    }

    private fun getConstructorSeasons(constructorStandingsWrapperList: List<ConstructorStandingsWrapper>): String {
        return constructorStandingsWrapperList.size.toString()
    }
}