package com.jorgenunezvazquez.formulastats.presentation.drivers

import androidx.navigation.fragment.findNavController
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.presentation.BaseFragment
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class DriverStandingsFragment : BaseFragment<DriverStandingsPresenter>(), DriversViewContract {

    override val presenter: DriverStandingsPresenter by inject { parametersOf(this) }

    override fun showDriversList(driverStandingsModel: List<DriverStandingsModel>) {
        val driverStandingsAdapter = DriverStandingsAdapter(driverStandingsModel) { driverId ->
            presenter.onDriverClicked(driverId)
        }
        binding.homeRecyclerView.adapter = driverStandingsAdapter
    }

    override fun goToDriverDetails(driverStandings: DriverStandings, round: String, season: String) {
        val directions = DriverStandingsFragmentDirections.actionDriversFragmentToDriverDetailActivity(DriverDetailParams(driverStandings, round, season))
        findNavController().navigate(directions)
    }
}


