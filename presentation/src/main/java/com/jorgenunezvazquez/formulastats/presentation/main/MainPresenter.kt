package com.jorgenunezvazquez.formulastats.presentation.main

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.useCases.main.LoadCountriesUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter

class MainPresenter(
    private val view: MainViewContract,
    private val loadCountriesUseCase: LoadCountriesUseCase,
    invoker: Invoker
) : BasePresenter(invoker) {

    override fun onPostCreate() {
        super.onPostCreate()
        loadCountries()
    }

    private fun loadCountries() {
        invoker.execute(loadCountriesUseCase) {
            view.setupView()
        }
    }
}

interface MainViewContract {
    fun setupView()
}