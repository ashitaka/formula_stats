package com.jorgenunezvazquez.formulastats.presentation.drivers

import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import java.io.Serializable

data class DriverDetailParams(
    val driverStandings: DriverStandings,
    val round: String,
    val season: String
): Serializable