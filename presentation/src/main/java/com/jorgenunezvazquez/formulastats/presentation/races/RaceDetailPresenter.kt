package com.jorgenunezvazquez.formulastats.presentation.races

import com.jorgenunezvazquez.formulastats.domain.DateUtils.getRaceOriginalDate
import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.useCases.races.GetDriverResultsByRaceParams
import com.jorgenunezvazquez.formulastats.domain.useCases.races.GetDriverResultsByRaceUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.races.RaceDetailModelHelper.createRaceDetailItemList
import java.text.SimpleDateFormat
import java.util.*

class RaceDetailPresenter(
    private val view: RaceDetailViewContract,
    private val getDriverResultsByRaceUseCase: GetDriverResultsByRaceUseCase,
    invoker: Invoker
) : BasePresenter(invoker) {

    override fun onPostCreate() {
        super.onPostCreate()
        view.getRaceParamsFromArgs()?.let { race ->
            view.initViews(race.raceName)
            val raceHasFinished = checkIfRaceHasFinished(race)
            view.updateRaceResults(createRaceDetailItemList(race, raceHasFinished = raceHasFinished))
            if (raceHasFinished) {
                getRaceResultsBySeason(race)
            }
        }
    }

    private fun getRaceResultsBySeason(race: Race) {
        view.showProgressBar()
        invoker.execute(getDriverResultsByRaceUseCase withParams GetDriverResultsByRaceParams(race)) {
            if (it is Success) {
                view.updateRaceResults(createRaceDetailItemList(race, it.data.driverResults, it.data.winnerImageUrl))
            } else {
                view.updateRaceResults(createRaceDetailItemList(race, raceHasFinished = false))
            }
            view.hideProgressBar()
        }
    }

    private fun checkIfRaceHasFinished(race: Race): Boolean = try {
        val currentDate = Calendar.getInstance().time
        val raceDate = getRaceOriginalDate(race)
        if (raceDate != null) {
            raceDate < currentDate
        } else {
            false
        }
    } catch (e: Exception) {
        false
    }
}

interface RaceDetailViewContract {
    fun getRaceParamsFromArgs(): Race?
    fun initViews(raceName: String)
    fun updateRaceResults(raceDetailsItemList: MutableList<AdapterItemModel>)
    fun showError()
    fun showProgressBar()
    fun hideProgressBar()
}