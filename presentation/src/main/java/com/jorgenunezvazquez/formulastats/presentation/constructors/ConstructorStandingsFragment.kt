package com.jorgenunezvazquez.formulastats.presentation.constructors

import androidx.navigation.fragment.findNavController
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.presentation.BaseFragment
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class ConstructorStandingsFragment : BaseFragment<ConstructorStandingsPresenter>(), ConstructorsViewContract {

    override val presenter: ConstructorStandingsPresenter by inject { parametersOf(this) }

    override fun showConstructorList(constructorStandingsModel: List<ConstructorStandingsModel>) {
        val constructorStandingsAdapter = ConstructorStandingsAdapter(constructorStandingsModel) { constructorStandings ->
            presenter.onConstructorClicked(constructorStandings)
        }
        binding.homeRecyclerView.adapter = constructorStandingsAdapter
    }

    override fun goToConstructorDetails(constructorStandings: ConstructorStandings, round: String, season: String) {
        val constructorDetailParams = ConstructorDetailParams(constructorStandings, round, season)
        val directions = ConstructorStandingsFragmentDirections.actionConstructorsFragmentToConstructorDetailActivity(constructorDetailParams)
        findNavController().navigate(directions)
    }
}


