package com.jorgenunezvazquez.formulastats.presentation.constructors

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemConstructorsBinding

class ConstructorStandingsAdapter(
    private val constructorStandingsList: List<ConstructorStandingsModel>,
    private val onDriverClickListener: (ConstructorStandingsModel) -> Unit
) : RecyclerView.Adapter<ConstructorStandingsAdapter.ConstructorStandingViewHolder>() {

    class ConstructorStandingViewHolder constructor(private val viewBinding: ItemConstructorsBinding) : RecyclerView.ViewHolder(viewBinding.root) {

        fun bindDriver(constructorStandingsModel: ConstructorStandingsModel, onConstructorClickListener: (ConstructorStandingsModel) -> Unit) {
            with(constructorStandingsModel) {
                viewBinding.constructorPosition.text = constructorPosition
                viewBinding.constructorPoints.text = constructorPoints
                viewBinding.constructorName.text = constructorName
                viewBinding.constructorDrivers.text = constructorDrivers
                viewBinding.constructorMarker.setImageDrawable(constructorMarker)

                viewBinding.constructorImageItem.setImageResource(constructorLogo)
                viewBinding.constructorImageItem.background.setTint(ContextCompat.getColor(itemView.context, constructorBackgroundColor))

                viewBinding.constructorItem.setOnClickListener {
                    onConstructorClickListener(this)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ConstructorStandingViewHolder {
        return ConstructorStandingViewHolder(ItemConstructorsBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))
    }

    override fun onBindViewHolder(constructorStandingViewHolder: ConstructorStandingViewHolder, i: Int) {
        constructorStandingViewHolder.bindDriver(constructorStandingsList[i], onDriverClickListener)
    }

    override fun getItemCount(): Int {
        return constructorStandingsList.size
    }
}
