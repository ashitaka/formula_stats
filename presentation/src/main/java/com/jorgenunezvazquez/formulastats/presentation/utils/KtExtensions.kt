package com.jorgenunezvazquez.formulastats.presentation.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun Context.showToast(@StringRes stringId: Int, time: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, resources.getString(stringId), time).show()
}
