package com.jorgenunezvazquez.formulastats.presentation.constructors

import android.content.Context
import android.graphics.drawable.Drawable
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.presentation.ConstructorStyle
import com.jorgenunezvazquez.formulastats.presentation.ConstructorMarkerUtils
import com.jorgenunezvazquez.formulastats.presentation.R
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.Serializable

data class ConstructorStandingsModel(
    val constructorPosition: String,
    val constructorPoints: String,
    val constructorName: String,
    val constructorDrivers: String,
    val constructorMarker: Drawable?,
    val constructorLogo: Int,
    val constructorBackgroundColor: Int,
    val flagImage: String
) : Serializable

object ConstructorStandingsModelHelper: KoinComponent {

    fun getConstructorListModel(constructorStandings: List<ConstructorStandings>): List<ConstructorStandingsModel> {
        return mutableListOf<ConstructorStandingsModel>().apply {
            constructorStandings.forEach { constructorStanding ->
                add(getConstructorStandingsModel(constructorStanding))
            }
        }
    }

    private fun getConstructorStandingsModel(constructorStanding: ConstructorStandings): ConstructorStandingsModel {
        val context: Context by inject()
        with(constructorStanding) {
            return ConstructorStandingsModel(
                constructorPosition = position,
                constructorPoints = String.format(context.resources.getString(R.string.points_sufix), points),
                constructorName = constructor.name,
                constructorDrivers = constructorStanding.driverStandings.joinToString(separator = "-") { it.driver.familyName },
                constructorMarker = ConstructorMarkerUtils.getConstructorMarker(constructor.constructorId),
                constructorLogo = ConstructorStyle.getConstructorLogoById(constructorStanding.constructor.constructorId),
                constructorBackgroundColor = ConstructorStyle.getConstructorLogoBackgroundColorById(constructorStanding.constructor.constructorId),
                flagImage = constructor.flagUrl
            )
        }
    }
}