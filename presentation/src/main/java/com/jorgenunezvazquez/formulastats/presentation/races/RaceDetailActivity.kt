package com.jorgenunezvazquez.formulastats.presentation.races

import android.os.Bundle
import android.view.MenuItem
import androidx.core.app.NavUtils
import androidx.navigation.ActivityNavigator
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.presentation.BaseActivity
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.databinding.ActivityRaceDetailBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.FirebaseUtils
import com.jorgenunezvazquez.formulastats.presentation.utils.hide
import com.jorgenunezvazquez.formulastats.presentation.utils.showToast
import com.jorgenunezvazquez.formulastats.presentation.utils.visible
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class RaceDetailActivity : BaseActivity<RaceDetailPresenter>(), RaceDetailViewContract {

    override val presenter: RaceDetailPresenter by inject { parametersOf(this) }
    private var raceDetailAdapter: RacesDetailAdapter? = null
    private lateinit var binding: ActivityRaceDetailBinding

    companion object {
        private const val ACTIVITY_NAME = "RaceDetail"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRaceDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.myToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        FirebaseUtils.sendViewSelectedEvent(ACTIVITY_NAME, this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initViews(raceName: String) {
        supportActionBar?.title = raceName
        raceDetailAdapter = RacesDetailAdapter()
        binding.detailRecyclerView.adapter = raceDetailAdapter
    }

    override fun updateRaceResults(raceDetailsItemList: MutableList<AdapterItemModel>) {
        raceDetailAdapter?.updateContent(raceDetailsItemList)
    }

    override fun showProgressBar() {
        binding.detailProgressBar.visible()
    }

    override fun hideProgressBar() {
        binding.detailProgressBar.hide()
    }

    override fun showError() {
        hideProgressBar()
        this.showToast(R.string.error_retrieving_data)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    override fun getRaceParamsFromArgs(): Race? {
        return intent.extras?.let { RaceDetailActivityArgs.fromBundle(it).raceParam }
    }
}
