package com.jorgenunezvazquez.formulastats.presentation.constructors

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.useCases.constructors.GetConstructorSeasonStandingsUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.HomeBaseViewContract
import com.jorgenunezvazquez.formulastats.presentation.constructors.ConstructorStandingsModelHelper.getConstructorListModel
import org.koin.core.component.KoinComponent
import java.io.Serializable

class ConstructorStandingsPresenter(
    private val view: ConstructorsViewContract,
    private val getConstructorSeasonStandingsUseCase: GetConstructorSeasonStandingsUseCase,
    invoker: Invoker
) : BasePresenter(invoker), KoinComponent {

    private var constructorStandingsWrapper: ConstructorStandingsWrapper? = null

    override fun onViewCreated() {
        super.onViewCreated()
        getConstructorSeasonStandings()
    }

    override fun onSavedInstanceState(data: Serializable) {
        this.constructorStandingsWrapper = data as ConstructorStandingsWrapper
        view.hideLoader()
        view.showConstructorList(getConstructorListModel(data.constructorStandings))
    }

    override fun onSaveInstanceState(): Serializable? = constructorStandingsWrapper

    fun onConstructorClicked(constructorStandingsModel: ConstructorStandingsModel) {
        constructorStandingsWrapper?.let { constructorStandingsWrapper ->
            val constructorStandings = constructorStandingsWrapper.constructorStandings.first { it.position == constructorStandingsModel.constructorPosition }
            view.goToConstructorDetails(constructorStandings, constructorStandingsWrapper.round, constructorStandingsWrapper.season)
        }
    }

    private fun getConstructorSeasonStandings() {
        view.showLoader()
        invoker.execute(getConstructorSeasonStandingsUseCase) {
            if (it is Success) {
                constructorStandingsWrapper = it.data
                view.showConstructorList(getConstructorListModel(it.data.constructorStandings))
            } else {
                view.showError()
            }
            view.hideLoader()
        }
    }
}

interface ConstructorsViewContract : HomeBaseViewContract {
    fun showConstructorList(constructorStandingsModel: List<ConstructorStandingsModel>)
    fun goToConstructorDetails(constructorStandings: ConstructorStandings, round: String, season: String)
}