package com.jorgenunezvazquez.formulastats.presentation.drivers

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemDriverBinding
import com.squareup.picasso.Picasso

class DriverStandingsAdapter(
    private val driverList: List<DriverStandingsModel>,
    private val onDriverClickListener: (String) -> Unit
) : RecyclerView.Adapter<DriverStandingsAdapter.DriverStandingsViewHolder>() {

    class DriverStandingsViewHolder internal constructor(private val viewBinding: ItemDriverBinding) : RecyclerView.ViewHolder(viewBinding.root) {

        fun bindDriver(driverStandingsModel: DriverStandingsModel, onDriverClickListener: (String) -> Unit) {
            with(driverStandingsModel) {
                viewBinding.driverPosition.text = driverPosition
                viewBinding.driverPoints.text = driverPoints
                viewBinding.driverName.text = driverName
                viewBinding.driverConstructor.text = driverConstructor
                viewBinding.constructorMarker.setImageDrawable(constructorMarker)

                if (driverImage.isNotEmpty()) {
                    Picasso.get().load(driverImage).fit().noFade().centerCrop(Gravity.TOP).into(viewBinding.driverImageItem)
                }

                viewBinding.driverItem.setOnClickListener {
                    onDriverClickListener(driverId)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DriverStandingsViewHolder {
        return DriverStandingsViewHolder(ItemDriverBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))
    }

    override fun onBindViewHolder(driverStandingsViewHolder: DriverStandingsViewHolder, i: Int) {
        driverStandingsViewHolder.bindDriver(driverList[i], onDriverClickListener)
    }

    override fun getItemCount(): Int {
        return driverList.size
    }
}
