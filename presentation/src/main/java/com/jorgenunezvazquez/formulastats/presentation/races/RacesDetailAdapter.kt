package com.jorgenunezvazquez.formulastats.presentation.races

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jorgenunezvazquez.formulastats.presentation.R
import com.jorgenunezvazquez.formulastats.presentation.databinding.ItemRaceResultBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.RaceDetailGpInfoItemBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.RaceDetailNoResultsItemBinding
import com.jorgenunezvazquez.formulastats.presentation.databinding.RaceDetailWinnerItemBinding
import com.jorgenunezvazquez.formulastats.presentation.utils.hide
import com.jorgenunezvazquez.formulastats.presentation.utils.visible
import com.squareup.picasso.Picasso

class RacesDetailAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    private var raceDetailModel = mutableListOf<AdapterItemModel>()

    fun updateContent(raceDetailModel: List<AdapterItemModel>) {
        this.raceDetailModel.clear()
        this.raceDetailModel.addAll(raceDetailModel)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return raceDetailModel[position].getItemViewType()
    }

    override fun getItemId(position: Int): Long {
        return raceDetailModel[position].getItemViewId()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            RaceDetailItemViewType.GP_INFO_ITEM.viewType -> createGPInfoViewHolder(parent)
            RaceDetailItemViewType.WINNER_INFO_ITEM.viewType -> createWinnerInfoViewHolder(parent)
            RaceDetailItemViewType.DRIVER_RESULT_ITEM.viewType -> createDriverResultViewHolder(
                parent
            )

            RaceDetailItemViewType.NO_RESULTS_ITEM.viewType -> createNoResultsViewHolder(parent)
            else -> error("Unknown item view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is GPInfoViewHolder -> holder.bind(raceDetailModel[position] as GPInfoModel)
            is WinnerInfoViewHolder -> holder.bind(raceDetailModel[position] as WinnerInfoModel)
            is DriverResultViewHolder -> holder.bind(raceDetailModel[position] as DriverResultModel)
            is NoResultsViewHolder -> holder.bind(raceDetailModel[position] as NoResultsModel)
        }
    }

    override fun getItemCount(): Int {
        return raceDetailModel.size
    }

    private fun createGPInfoViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return GPInfoViewHolder(
            RaceDetailGpInfoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createWinnerInfoViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return WinnerInfoViewHolder(
            RaceDetailWinnerItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createDriverResultViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return DriverResultViewHolder(
            ItemRaceResultBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    private fun createNoResultsViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return NoResultsViewHolder(
            RaceDetailNoResultsItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class GPInfoViewHolder internal constructor(private val viewBinding: RaceDetailGpInfoItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(gpInfoModel: GPInfoModel) {
            with(viewBinding) {
                if (gpInfoModel.raceImage.isNotEmpty()) {
                    Picasso.get().load(gpInfoModel.raceImage).fit().centerInside()
                        .into(raceImageItem)
                } else {
                    raceImageItem.hide()
                }
                if (gpInfoModel.flagImage.isNotEmpty()) {
                    Picasso.get().load(gpInfoModel.flagImage).fit().centerCrop().into(raceFlag)
                }
                raceName.text = gpInfoModel.raceName
                circuitName.text = gpInfoModel.circuitName
                locality.text = gpInfoModel.locality
                date.text = gpInfoModel.raceDate
                time.text = gpInfoModel.raceTime
                raceImageItem.background = ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.next_gp_gradient_background
                )
            }
        }
    }

    class NoResultsViewHolder internal constructor(private val viewBinding: RaceDetailNoResultsItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(noResultsModel: NoResultsModel) {
            with(viewBinding) {
                noResultsTitle.text = noResultsModel.screenMessage
            }
        }
    }

    class WinnerInfoViewHolder internal constructor(private val viewBinding: RaceDetailWinnerItemBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(winnerInfoModel: WinnerInfoModel) {
            with(viewBinding) {
                raceSeasonTitle.text = winnerInfoModel.raceSeason
                Picasso.get().load(winnerInfoModel.winnerImage).fit().noFade()
                    .centerCrop(Gravity.TOP).into(winnerImageView)
                winnerName.text = winnerInfoModel.winnerName
                winnerTeamMarker.setImageDrawable(winnerInfoModel.winnerTeamMarker)
                pole.text = winnerInfoModel.poleDriver
                poleTeamMarker.setImageDrawable(winnerInfoModel.poleTeamMarker)
                flap.text = winnerInfoModel.fastestLapDriver
                fLapTeamMarker.setImageDrawable(winnerInfoModel.fastestLapTeamMarker)
                laps.text = winnerInfoModel.totalLaps
                lapTime.text = winnerInfoModel.lapTime
            }
        }
    }

    class DriverResultViewHolder internal constructor(private val viewBinding: ItemRaceResultBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(driverResultModel: DriverResultModel) {
            with(viewBinding) {
                driverName.text = driverResultModel.driverName
                driverPoints.text = driverResultModel.driverPoints
                driverPosition.text = driverResultModel.driverPosition
                driverTeam.text = driverResultModel.driverTeam
                driverTeamMarker.setImageDrawable(driverResultModel.driverTeamMarker)
                driverTime.text = driverResultModel.driverTime
                if (driverResultModel.showHeader) {
                    driverResultHeader.visible()
                } else {
                    driverResultHeader.hide()
                }
            }
        }
    }
}
