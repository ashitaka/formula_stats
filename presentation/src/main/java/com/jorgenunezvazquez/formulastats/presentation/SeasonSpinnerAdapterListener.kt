package com.jorgenunezvazquez.formulastats.presentation

import android.view.View
import android.widget.AdapterView

class SeasonSpinnerAdapterListener(private val seasonListener: (String) -> Unit) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        val seasonSelected = p0?.getItemAtPosition(p2).toString()
        seasonListener.invoke(seasonSelected)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) = Unit
}