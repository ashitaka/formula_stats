package com.jorgenunezvazquez.formulastats.presentation.drivers

import com.jorgenunezvazquez.formulastats.domain.Invoker
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandings
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.useCases.drivers.GetDriversCurrentSeasonStandingsUseCase
import com.jorgenunezvazquez.formulastats.presentation.BasePresenter
import com.jorgenunezvazquez.formulastats.presentation.HomeBaseViewContract
import com.jorgenunezvazquez.formulastats.presentation.drivers.DriverStandingsModelHelper.getDriverListModel
import java.io.Serializable

class DriverStandingsPresenter(
    private val view: DriversViewContract,
    private val getDriversCurrentSeasonStandingsUseCase: GetDriversCurrentSeasonStandingsUseCase,
    invoker: Invoker
) : BasePresenter(invoker) {

    private var driverStandingsWrapper: DriverStandingsWrapper? = null

    override fun onViewCreated() {
        super.onViewCreated()
        getDriverSeasonStandings()
    }

    override fun onSavedInstanceState(data: Serializable) {
        this.driverStandingsWrapper = data as DriverStandingsWrapper
        view.hideLoader()
        view.showDriversList(getDriverListModel(data.driverStandings))
    }

    override fun onSaveInstanceState(): Serializable? = driverStandingsWrapper

    fun onDriverClicked(driverId: String) {
        driverStandingsWrapper?.let { driverStandingsWrapper ->
            val driverStanding = driverStandingsWrapper.driverStandings.first { it.driver.driverId == driverId }
            view.goToDriverDetails(driverStanding, driverStandingsWrapper.round, driverStandingsWrapper.season)
        }
    }

    private fun getDriverSeasonStandings() {
        view.showLoader()
        invoker.execute(getDriversCurrentSeasonStandingsUseCase) {
            if (it is Success) {
                driverStandingsWrapper = it.data
                view.showDriversList(getDriverListModel(it.data.driverStandings))
            } else {
                view.showError()
            }
            view.hideLoader()
        }
    }
}

interface DriversViewContract : HomeBaseViewContract {
    fun showDriversList(driverStandingsModel: List<DriverStandingsModel>)
    fun goToDriverDetails(driverStandings: DriverStandings, round: String, season: String)
}