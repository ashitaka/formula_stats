# Formula Stats
F1 Race Calendar and Historical Stats

## Description
Formula Stats is an Android app aimed at Formula 1 lovers. Its main features are:

- GP Calendar of the current Formula 1 championship
- Current Formula 1 championship race results
- Historical stats of the drivers
- Historical stats of the constructors
- Cool design
- Completely Free
- No Ads

<img src="main-app/src/main/assets/gp_shot.png" width="300" height="615">
<img src="main-app/src/main/assets/drivers_shot.png" width="300" height="615">
<img src="main-app/src/main/assets/constructors_shot.png" width="300" height="615">
<img src="main-app/src/main/assets/alonso_shot.png" width="300" height="615">
<img src="main-app/src/main/assets/williams_shot.png" width="300" height="615">

## Support
If you want to contact me please write to: ashitakadev@gmail.com

## Contributing
I would like to thank the efforts of the people that helped to create this app. The data is obtained thanks to the <a href="http://ergast.com/mrd/" title="Ergast">Ergast API</a>, the <a href="https://www.mediawiki.org/wiki/API:REST_API/Reference" title="MediaWiki">MediaWiki API</a> and the <a href="https://www.countryflagsapi.com/" title="CountryFlags">CountryFlags API</a>

The icons and logos were obtained from FlatIcon:

<a href="https://www.flaticon.com/free-icons/helmet" title="helmet icons">Helmet icons created by Freepik - Flaticon</a><br>
<a href="https://www.flaticon.com/free-icons/flag" title="flag icons">Flag icons created by Freepik - Flaticon</a><br>
<a href="https://www.flaticon.es/iconos-gratis/trabajo" title="trabajo iconos">Work icon created by Retinaicons - Flaticon</a><br>
<a href="https://www.flaticon.com/free-icons/finish" title="finish icons">Finish icons created by yut1655 - Flaticon</a><br>

The animations were obtained from LottieFiles, under the Lottie Simple License :

Finish flags by Makes, https://lottiefiles.com/makas <br>
Happy Mechanic by Rishabh Goel, https://lottiefiles.com/rishabhgoel97 <br>
Smoking wheel by Mike Mk, https://lottiefiles.com/user/775169 <br>

Preview screenshots by https://previewed.app/template/16DCE402

## License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

## Disclaimer
This app is unofficial and is not associated in any way with the Formula One group or companies. F1, FORMULA ONE, FORMULA 1, FIA FORMULA ONE CHAMPIONSHIP, GRAND PRIX and related marks are trade marks of Formula One Licensing B.V.

## Project status
New features in progress