package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorRemoteData
import com.google.gson.annotations.SerializedName

data class DriverStandingsRemoteData(
    @SerializedName("position") val position: String,
    @SerializedName("positionText") val positionText: String,
    @SerializedName("points") val points: String,
    @SerializedName("wins") val wins: String,
    @SerializedName("Driver") val driverRemoteData: DriverRemoteData,
    @SerializedName("Constructors") val constructorRemoteDataList: List<ConstructorRemoteData>
)