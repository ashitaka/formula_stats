package com.jorgenunezvazquez.formulastats.remotedatasource.api


import com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries.CountryRemoteData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface CountriesApi {

    companion object {
        const val BASE_URL_COUNTRIES = "https://restcountries.com/v2/"
    }

    @GET("all")
    fun getAllCountryCodes(@QueryMap queryMap: Map<String, String>): Call<List<CountryRemoteData>>
}
