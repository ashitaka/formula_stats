package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors

import com.google.gson.annotations.SerializedName

class ConstructorStandingRemote(
    @SerializedName("position") val position: String,
    @SerializedName("positionText") val positionText: String,
    @SerializedName("points") val points: String,
    @SerializedName("wins") val wins: String,
    @SerializedName("Constructor") val constructor: ConstructorRemoteData
)