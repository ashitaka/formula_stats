package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.dataWrapper

import com.google.gson.annotations.SerializedName
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorStandingRemote
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.DriverRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.DriverStandingsRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.DriversRaceResultsRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.races.RaceRemoteData

data class DataWrapper<T>(@SerializedName("MRData") val mrData: T)

/************************** DriverSeasonStandings*******************************/

data class DriverSeasonStandingsData(
    @SerializedName("StandingsTable") val tableDriver: DriverSeasonsStandingsTable
)

data class DriverSeasonsStandingsTable(
    @SerializedName("season") val season: String,
    @SerializedName("StandingsLists") val standingList: List<DriverStandingsRemote>
)

/************************** AllDrivers *******************************/

data class AllDriversData(
    @SerializedName("DriverTable") val table: AllDriversTable
)


data class AllDriversTable(
    @SerializedName("Drivers") val driverList: List<DriverRemoteData>
)

/************************** DriverStandingsById*******************************/

data class DriverStandingsData(
    @SerializedName("StandingsTable") val table: DriversStandingsTable
)

data class DriversStandingsTable(
    @SerializedName("season") val season: String,
    @SerializedName("driverId") val driverId: String,
    @SerializedName("StandingsLists") val standingList: List<DriverStandingsRemote>
)

data class DriverStandingsRemote(
    @SerializedName("season") val season: String,
    @SerializedName("round") val round: String,
    @SerializedName("DriverStandings") val driverStandingsRemoteDataList: List<DriverStandingsRemoteData>
)

/************************** DriverResults*******************************/

data class DriverRaceResultsData(
    @SerializedName("RaceTable") val table: DriverRaceTable
)

data class DriverRaceTable(
    @SerializedName("driverId") val driverId: String,
    @SerializedName("Races") val races: List<DriversRaceResultsRemoteData>
)

/************************** ConstructorSeasonStandings*******************************/

data class ConstructorSeasonStandingsData(
    @SerializedName("StandingsTable") val table: ConstructorSeasonStandingsTable
)


data class ConstructorSeasonStandingsTable(
    @SerializedName("season") val season: String,
    @SerializedName("StandingsLists") val standingList: List<ConstructorSeasonStandings>
)

data class ConstructorSeasonStandings(
    @SerializedName("season") val season: String,
    @SerializedName("round") val round: String,
    @SerializedName("ConstructorStandings") val constructorStandings: List<ConstructorStandingRemote>
)

/************************** AllConstructors *******************************/

data class AllConstructorsData(
    @SerializedName("ConstructorTable") val table: AllConstructorsTable
)


data class AllConstructorsTable(
    @SerializedName("Constructors") val constructorList: List<ConstructorRemoteData>
)

/************************** ConstructorStandingsById*******************************/

data class ConstructorStandingsData(
    @SerializedName("StandingsTable") val table: ConstructorStandingsTable
)

data class ConstructorStandingsTable(
    @SerializedName("constructorId") val constructorId: String,
    @SerializedName("StandingsLists") val standingList: List<ConstructorStandingsWrapperRemote>
)

data class ConstructorStandingsWrapperRemote(
    @SerializedName("season") val season: String,
    @SerializedName("round") val round: String,
    @SerializedName("ConstructorStandings") val constructorStandings: List<ConstructorStandingRemote>
)

/************************** RaceSchedule *******************************/

data class RaceScheduleData(
    @SerializedName("RaceTable") val raceTable: RaceTable
)

data class RaceTable(
    @SerializedName("season") val season: String,
    @SerializedName("Races") val raceList: List<RaceRemoteData>
)

/************************** RaceSchedule *******************************/

data class RaceResultsData(
    @SerializedName("RaceTable") val raceTable: RaceResultsTable
)

data class RaceResultsTable(
    @SerializedName("season") val season: String,
    @SerializedName("circuitId") val circuitId: String,
    @SerializedName("Races") val raceList: List<RaceRemoteData>
)






