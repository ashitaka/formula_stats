package com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia

object ImagesWebServiceHelper {

    fun query(pageTitle: String): Map<String, String?> {
        return mapOf(
            "action" to "query",
            "prop" to "pageimages",
            "format" to "json",
            "pithumbsize" to "500",
            "formatversion" to "2",
            "titles" to pageTitle,
            "redirects" to ""
        )
    }
}