package com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.ImagesRemoteDataSource
import com.jorgenunezvazquez.formulastats.remotedatasource.api.WikiApi
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toWikiSource
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ImagesRemoteDataSourceImpl : ImagesRemoteDataSource, KoinComponent {

    private val wikiApi: WikiApi by inject()

    override fun getImageFromWiki(pageTitle: String): String = try {
        val response = wikiApi.getImageUrl(ImagesWebServiceHelper.query(pageTitle)).execute()
        if (response.isSuccessful) {
            response.body()?.toWikiSource()?.let { return it }
        }
        throw Exception()

    } catch (e: Exception) {
        throw Exception(e)
    }
}


















