package com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries

import com.google.gson.annotations.SerializedName

data class CountryRemoteData(
        @SerializedName("name") val name: String,
        @SerializedName("alpha2Code") val alpha2Code: String,
        @SerializedName("demonym") val demonym: String
)