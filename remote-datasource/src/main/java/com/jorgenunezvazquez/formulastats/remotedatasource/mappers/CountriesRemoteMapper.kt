package com.jorgenunezvazquez.formulastats.remotedatasource.mappers

import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries.CountryRemoteData


fun List<CountryRemoteData>.toCountryDataList() = map { it.toCountryData() }

private fun CountryRemoteData.toCountryData() = CountryData(name, alpha2Code, demonym)


