package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.races

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.CircuitRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.DriverResultsData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.LocationRemoteData
import com.google.gson.annotations.SerializedName

class RaceRemoteData(
    @SerializedName("season") val season: String = "",
    @SerializedName("round") val round: String = "",
    @SerializedName("url") val url: String? = "",
    @SerializedName("raceName") val raceName: String = "",
    @SerializedName("Circuit") val circuitRemoteData: CircuitRemoteData = CircuitRemoteData(locationRemoteData = LocationRemoteData()),
    @SerializedName("date") val date: String = "",
    @SerializedName("time") val time: String?,
    @SerializedName("Results") val results: List<DriverResultsData>?
)

