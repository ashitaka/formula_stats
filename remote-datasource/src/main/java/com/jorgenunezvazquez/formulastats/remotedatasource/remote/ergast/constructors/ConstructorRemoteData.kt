package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors

import com.google.gson.annotations.SerializedName

data class ConstructorRemoteData(
    @SerializedName("constructorId") val constructorId: String,
    @SerializedName("url") val url: String,
    @SerializedName("name") val name: String,
    @SerializedName("nationality") val nationality: String
)