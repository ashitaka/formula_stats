package com.jorgenunezvazquez.formulastats.remotedatasource.injection

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.CountriesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.remote.ImagesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.remote.StatsRemoteDataSource
import com.jorgenunezvazquez.formulastats.remotedatasource.NetworkApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.CountriesApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.CountriesApi.Companion.BASE_URL_COUNTRIES
import com.jorgenunezvazquez.formulastats.remotedatasource.api.ErgastApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.ErgastApi.Companion.BASE_URL
import com.jorgenunezvazquez.formulastats.remotedatasource.api.WikiApi
import com.jorgenunezvazquez.formulastats.remotedatasource.api.WikiApi.Companion.BASE_URL_WIKI
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries.CountriesRemoteDataSourceImpl
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.StatsRemoteDataSourceImpl
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia.ImagesRemoteDataSourceImpl
import org.koin.dsl.module

object RemoteModules {

    val remoteModules = module {
        factory<CountriesRemoteDataSource> { CountriesRemoteDataSourceImpl() }
        factory<StatsRemoteDataSource> { StatsRemoteDataSourceImpl() }
        factory<ImagesRemoteDataSource> { ImagesRemoteDataSourceImpl() }

        single { NetworkApi().provideApi(BASE_URL, ErgastApi::class.java) }
        single { NetworkApi().provideApi(BASE_URL_COUNTRIES, CountriesApi::class.java) }
        single { NetworkApi().provideApi(BASE_URL_WIKI, WikiApi::class.java) }
    }
}