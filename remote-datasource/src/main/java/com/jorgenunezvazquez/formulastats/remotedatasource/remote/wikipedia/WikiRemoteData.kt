package com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia

import com.google.gson.annotations.SerializedName


data class WikiResponse(
        @SerializedName("batchcomplete") val batchComplete: Boolean,
        @SerializedName("query") val query: Query
)

data class Query(
        @SerializedName("normalized") val normalizedLIst: List<Normalized>,
        @SerializedName("pages") val pages: List<PageDetail>
)

data class Normalized(
        @SerializedName("fromencoded") val fromEncoded: Boolean,
        @SerializedName("from") val from: String,
        @SerializedName("to") val to: String
)

data class PageDetail(
        @SerializedName("pageid") val pageId: String,
        @SerializedName("ns") val ns: String,
        @SerializedName("title") val title: String,
        @SerializedName("thumbnail") val thumbnail: Thumbnail,
        @SerializedName("pageimage") val image: String
)

data class Thumbnail(
        @SerializedName("source") val source: String,
        @SerializedName("width") val width: String,
        @SerializedName("height") val height: String
)