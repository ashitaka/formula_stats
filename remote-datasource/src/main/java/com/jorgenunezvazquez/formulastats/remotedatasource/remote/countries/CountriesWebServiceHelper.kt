package com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries

object CountriesWebServiceHelper {

    fun query(): Map<String, String> {
        return mapOf("fields" to "name,alpha2Code,demonym")
    }
}