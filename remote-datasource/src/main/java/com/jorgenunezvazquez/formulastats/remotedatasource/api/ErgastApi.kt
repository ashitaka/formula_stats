package com.jorgenunezvazquez.formulastats.remotedatasource.api

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.dataWrapper.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ErgastApi {

    companion object {
        const val BASE_URL = "https://api.jolpi.ca/ergast/f1/"
    }

    @GET("{season}/driverStandings.json")
    fun getDriversSeasonStandings(@Path("season") season: String?): Call<DataWrapper<DriverSeasonStandingsData>>

    @GET("{season}/drivers/{driverId}/driverStandings.json")
    fun getDriverStandingsById(@Path("season") season: String?, @Path("driverId") driverId: String?): Call<DataWrapper<DriverStandingsData>>

    @GET("drivers/{driverId}/driverStandings/1.json")
    fun getDriverChampionships(@Path("driverId") driverId: String?): Call<DataWrapper<DriverSeasonStandingsData>>

    @GET("drivers/{driverId}/results.json?limit=500")
    fun getAllDriverResults(@Path("driverId") driverId: String?): Call<DataWrapper<DriverRaceResultsData>>

    @GET("{season}/drivers/{driverId}/results.json")
    fun getDriverResultsBySeason(@Path("season") season: String?, @Path("driverId") driverId: String?): Call<DataWrapper<DriverRaceResultsData>>

    @GET("{season}/constructorStandings.json")
    fun getConstructorSeasonStandings(@Path("season") season: String?): Call<DataWrapper<ConstructorSeasonStandingsData>>

    @GET("{season}/constructors/{constructorId}/constructorStandings.json")
    fun getConstructorStandingsById(@Path("season") season: String?, @Path("constructorId") driverId: String?): Call<DataWrapper<ConstructorSeasonStandingsData>>

    @GET("constructors/{constructorId}/constructorStandings.json?limit=100")
    fun getAllConstructorStandingsById(@Path("constructorId") constructorId: String?): Call<DataWrapper<ConstructorStandingsData>>

    @GET("{season}.json")
    fun getRaceSchedule(@Path("season") season: String?): Call<DataWrapper<RaceScheduleData>>

    @GET("{season}/circuits/{circuitId}/results.json")
    fun getDriverResultsBySeasonAndCircuit(@Path("season") season: String?, @Path("circuitId") circuitId: String?): Call<DataWrapper<RaceResultsData>>
}