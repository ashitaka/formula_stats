package com.jorgenunezvazquez.formulastats.remotedatasource

import okhttp3.OkHttpClient
import org.koin.core.component.KoinComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkApi : KoinComponent {

    fun <T> provideApi(
        baseUrl: String,
        clazz: Class<T>
    ): T = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(OkHttpClient.Builder().build())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(clazz)
}