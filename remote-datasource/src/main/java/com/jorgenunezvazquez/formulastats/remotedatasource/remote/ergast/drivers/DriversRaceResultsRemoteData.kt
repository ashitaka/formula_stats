package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers

import com.google.gson.annotations.SerializedName
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorRemoteData

data class DriversRaceResultsRemoteData(
    @SerializedName("season") val season: String,
    @SerializedName("round") val round: String,
    @SerializedName("url") val url: String,
    @SerializedName("raceName") val raceName: String,
    @SerializedName("Circuit") val circuitRemoteData: CircuitRemoteData,
    @SerializedName("date") val date: String,
    @SerializedName("Results") val results: List<DriverResultsData>
)

data class CircuitRemoteData(
    @SerializedName("circuitId") val circuitId: String = "",
    @SerializedName("url") val url: String = "",
    @SerializedName("circuitName") val circuitName: String = "",
    @SerializedName("Location") val locationRemoteData: LocationRemoteData
)

data class LocationRemoteData(
    @SerializedName("lat") val lat: String = "",
    @SerializedName("long") val long: String = "",
    @SerializedName("locality") val locality: String = "",
    @SerializedName("country") val country: String = ""
)

data class TimeRemoteData(
    @SerializedName("millis") val millis: String,
    @SerializedName("time") val time: String
)

data class FastestLapRemoteData(
    @SerializedName("rank") val rank: String,
    @SerializedName("lap") val lap: String,
    @SerializedName("Time") val fastestLapTimeRemoteData: FastestLapTimeRemoteData,
    @SerializedName("AverageSpeed") val averageSpeed: AverageSpeedRemoteData
)

data class FastestLapTimeRemoteData(
    @SerializedName("time") val time: String
)

data class AverageSpeedRemoteData(
    @SerializedName("units") val units: String,
    @SerializedName("speed") val speed: String
)

data class DriverResultsData(
    @SerializedName("number") val number: String,
    @SerializedName("position") val position: String,
    @SerializedName("points") val points: String,
    @SerializedName("Driver") val driverRemoteData: DriverRemoteData,
    @SerializedName("Constructor") val constructorRemoteData: ConstructorRemoteData,
    @SerializedName("grid") val grid: String,
    @SerializedName("laps") val laps: String,
    @SerializedName("status") val status: String,
    @SerializedName("Time") val timeRemoteData: TimeRemoteData?,
    @SerializedName("FastestLap") val fastestLapRemoteData: FastestLapRemoteData?
)