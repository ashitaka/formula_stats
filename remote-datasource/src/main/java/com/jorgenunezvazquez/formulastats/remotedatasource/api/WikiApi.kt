package com.jorgenunezvazquez.formulastats.remotedatasource.api


import com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia.WikiResponse

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface WikiApi {

    companion object {
        const val BASE_URL_WIKI = "http://en.wikipedia.org/w/"
    }

    @GET("api.php")
    fun getImageUrl(@QueryMap(encoded = true) queryMap: Map<String, String?>): Call<WikiResponse>
}
