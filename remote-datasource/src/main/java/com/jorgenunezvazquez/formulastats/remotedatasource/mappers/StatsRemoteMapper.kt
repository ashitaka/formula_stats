package com.jorgenunezvazquez.formulastats.remotedatasource.mappers

import com.jorgenunezvazquez.formulastats.domain.model.constructors.Constructor
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandings
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.*
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorRemoteData
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.constructors.ConstructorStandingRemote
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.dataWrapper.*
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.drivers.*
import com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast.races.RaceRemoteData


fun DriverStandingsRemote.toDriverStandingsWrapper() = DriverStandingsWrapper(
    season,
    round,
    driverStandingsRemoteDataList.toDriverStandingsList()
)

fun ConstructorSeasonStandings.toConstructorStandingsWrapper() = ConstructorStandingsWrapper(
    season,
    round,
    constructorStandings.toConstructorStandingsList()
)

private fun ConstructorStandingsWrapperRemote.toConstructorStandingsWrapper() = ConstructorStandingsWrapper(
    season,
    round,
    constructorStandings.toConstructorStandingsList()
)

fun List<DriverResultsData>.toDriverResultsList() = map { it.toDriverResult() }

fun List<ConstructorRemoteData>.toConstructorList() = map { it.toConstructor() }

fun DataWrapper<RaceScheduleData>.toRaceDataList() = mrData.raceTable.raceList.map { raceListItem -> raceListItem.toRace() }

fun DataWrapper<RaceResultsData>.toDriverResultsList() = mrData.raceTable.raceList.first().results?.map { it.toDriverResult() } ?: emptyList()

fun DataWrapper<DriverSeasonStandingsData>.toDriverChampionships() = mrData.tableDriver.standingList.size

fun DataWrapper<DriverRaceResultsData>.toDriverRaceResultsList() = mrData.table.races.map { it.toDriverRaceResult() }

fun DataWrapper<DriverStandingsData>.toDriverStandingsWrapper() = mrData.table.standingList.first().toDriverStandingsWrapper()

fun DataWrapper<DriverSeasonStandingsData>.toDriverSeasonStandingsWrapper() = mrData.tableDriver.standingList.first().toDriverStandingsWrapper()

fun DataWrapper<ConstructorSeasonStandingsData>.toConstructorStandingsWrapper() = mrData.table.standingList.first().toConstructorStandingsWrapper()

fun DataWrapper<ConstructorStandingsData>.toConstructorStandingsWrapper() = mrData.table.standingList.map { it.toConstructorStandingsWrapper() }

private fun List<DriverStandingsRemoteData>.toDriverStandingsList() = map { it.toDriverStandings() }

private fun List<ConstructorStandingRemote>.toConstructorStandingsList() = map { it.toConstructorStandings() }

private fun RaceRemoteData.toRace(): Race {
    return Race(
        season,
        round,
        url.orEmpty(),
        raceName,
        circuitRemoteData.toCircuit(),
        date,
        time,
        results?.toDriverResultsList() ?: emptyList()
    )
}

private fun CircuitRemoteData.toCircuit(): Circuit {
    return Circuit(
        circuitId,
        url,
        circuitName,
        locationRemoteData.toLocation()
    )
}

private fun DriverRemoteData.toDriver() = Driver(
    driverId,
    permanentNumber.orEmpty(),
    code.orEmpty(),
    url.orEmpty(),
    givenName.orEmpty(),
    familyName.orEmpty(),
    dateOfBirth.orEmpty(),
    nationality.orEmpty()
)

private fun ConstructorRemoteData.toConstructor() = Constructor(
    constructorId,
    url,
    name,
    nationality
)

private fun DriverStandingsRemoteData.toDriverStandings() = DriverStandings(
    position,
    positionText,
    points,
    wins,
    driverRemoteData.toDriver(),
    constructorRemoteDataList.toConstructorList()
)

private fun ConstructorStandingRemote.toConstructorStandings() = ConstructorStandings(
    position,
    points,
    wins,
    constructor.toConstructor()
)

private fun LocationRemoteData.toLocation() = Location(
    lat,
    long,
    locality,
    country
)

private fun DriverResultsData.toDriverResult() = DriverResult(
    number,
    position,
    points,
    driverRemoteData.toDriver(),
    constructorRemoteData.toConstructor(),
    grid,
    laps,
    status,
    timeRemoteData?.toTime(),
    fastestLapRemoteData?.toFastestLap()
)

private fun DriversRaceResultsRemoteData.toDriverRaceResult() = DriverRaceResult(
    season,
    round,
    url,
    raceName,
    circuitRemoteData.toCircuit(),
    date,
    results.toDriverResultsList().first()
)

private fun TimeRemoteData.toTime() = Time(millis, time)

private fun FastestLapRemoteData.toFastestLap() = FastestLap(
    rank,
    lap,
    fastestLapTimeRemoteData.toFastestLapTime(),
    averageSpeed.toAverageSpeed()
)

private fun FastestLapTimeRemoteData.toFastestLapTime() = FastestLapTime(time)

private fun AverageSpeedRemoteData.toAverageSpeed() = AverageSpeed(units, speed)


