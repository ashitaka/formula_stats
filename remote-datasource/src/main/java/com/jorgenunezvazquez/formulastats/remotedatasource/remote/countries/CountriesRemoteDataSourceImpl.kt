package com.jorgenunezvazquez.formulastats.remotedatasource.remote.countries

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.CountriesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.remotedatasource.api.CountriesApi
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toCountryDataList
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class CountriesRemoteDataSourceImpl : CountriesRemoteDataSource, KoinComponent {

    private val api: CountriesApi by inject()

    override fun getCountries(): List<CountryData> = try {
        val response = api.getAllCountryCodes(CountriesWebServiceHelper.query()).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toCountryDataList() }
        }
        mutableListOf()
    } catch (e: Exception) {
        throw Exception(e)
    }
}