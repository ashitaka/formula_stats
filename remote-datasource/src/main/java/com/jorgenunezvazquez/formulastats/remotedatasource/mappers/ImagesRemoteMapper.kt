package com.jorgenunezvazquez.formulastats.remotedatasource.mappers

import com.jorgenunezvazquez.formulastats.remotedatasource.remote.wikipedia.WikiResponse

fun WikiResponse.toWikiSource() = query.pages.firstOrNull()?.thumbnail?.source

