package com.jorgenunezvazquez.formulastats.remotedatasource.remote.ergast

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.StatsRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.remotedatasource.api.ErgastApi
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toDriverChampionships
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toDriverRaceResultsList
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toDriverResultsList
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toDriverSeasonStandingsWrapper
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toDriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.remotedatasource.mappers.toRaceDataList
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

private const val CURRENT_SEASON_ID = "current"

class StatsRemoteDataSourceImpl : StatsRemoteDataSource, KoinComponent {

    private val api: ErgastApi by inject()

    override fun getCurrentRaceSchedule(): List<Race> = try {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR).toString()
        val response = api.getRaceSchedule(currentYear).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toRaceDataList() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception()
    }

    override fun getDriverResultsByRace(race: Race): List<DriverResult> = try {
        val response = api.getDriverResultsBySeasonAndCircuit(race.season, race.circuit.circuitId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverResultsList() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception()
    }

    override fun getDriverChampionships(driverId: String): Int = try {
        val response = api.getDriverChampionships(driverId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverChampionships() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getAllDriverRaceResults(driverId: String): List<DriverRaceResult> = try {
        val response = api.getAllDriverResults(driverId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverRaceResultsList() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getDriverStandingsBySeason(season: String, driverId: String): DriverStandingsWrapper = try {
        val response = api.getDriverStandingsById(season, driverId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverStandingsWrapper() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getDriversSeasonStandings(season: String): DriverStandingsWrapper = try {
        val response = api.getDriversSeasonStandings(season).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverSeasonStandingsWrapper() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getDriverResultsBySeason(season: String, driverId: String): List<DriverRaceResult> = try {
        val response = api.getDriverResultsBySeason(season, driverId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toDriverRaceResultsList() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getConstructorSeasonStandings(): ConstructorStandingsWrapper = try {
        val response = api.getConstructorSeasonStandings(CURRENT_SEASON_ID).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toConstructorStandingsWrapper() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception()
    }

    override fun getAllConstructorStandingsById(constructorId: String): List<ConstructorStandingsWrapper> = try {
        val response = api.getAllConstructorStandingsById(constructorId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toConstructorStandingsWrapper() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }

    override fun getConstructorStandingsBySeason(season: String, constructorId: String): ConstructorStandingsWrapper = try {
        val response = api.getConstructorStandingsById(season, constructorId).execute()
        if (response.isSuccessful) {
            response.body()?.let { return it.toConstructorStandingsWrapper() }
        }
        throw Exception()
    } catch (e: Exception) {
        throw Exception(e)
    }
}


















