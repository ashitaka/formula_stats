package com.jorgenunezvazquez.formulastats.data.repositories

import com.jorgenunezvazquez.formulastats.domain.datasources.cache.CacheDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.remote.ImagesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository

class ImagesRepositoryImpl(
    private val countriesLocalDataSource: CountriesLocalDataSource,
    private val imagesRemoteDataSource: ImagesRemoteDataSource,
    private val cacheDataSource: CacheDataSource
) : ImagesRepository {

    override fun getImageUrlFromWiki(pageTitle: String): Result<String> = try {
        if (!cacheDataSource.isImageUrlCached(pageTitle)) {
            imagesRemoteDataSource.getImageFromWiki(pageTitle).let {
                if (it.isNotBlank()) {
                    cacheDataSource.putImageUrl(pageTitle, it)
                }
            }
        }
        Success(cacheDataSource.getImageUrl(pageTitle))
    } catch (e: Exception) {
        Error()
    }

    override fun getFlagByNationality(nationality: String): Result<String> = try {
        Success(countriesLocalDataSource.getCountryFlagUrlByNationalityString(nationality))
    } catch (e: Exception) {
        Error()
    }

    override fun getFlagByCountry(country: String): Result<String> = try {
        Success(countriesLocalDataSource.getCountryFlagUrlByCountry(country))
    } catch (e: Exception) {
        Error()
    }
}