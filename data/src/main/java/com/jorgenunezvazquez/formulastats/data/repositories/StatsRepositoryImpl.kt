package com.jorgenunezvazquez.formulastats.data.repositories

import com.jorgenunezvazquez.formulastats.domain.datasources.remote.StatsRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.model.constructors.ConstructorStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverRaceResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverResult
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverStandingsWrapper
import com.jorgenunezvazquez.formulastats.domain.model.drivers.DriverTotalResult
import com.jorgenunezvazquez.formulastats.domain.model.races.Race
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository

class StatsRepositoryImpl(private val remoteDataSource: StatsRemoteDataSource) : StatsRepository {

    override fun getCurrentRaceSchedule(): Result<List<Race>> = try {
        val races = remoteDataSource.getCurrentRaceSchedule()
        if (races.isNotEmpty()) {
            Success(races)
        } else {
            Error()
        }
    } catch (e: Exception) {
        Error(e)
    }

    override fun getDriverResultsByRace(race: Race): Result<List<DriverResult>> = try {
        Success(remoteDataSource.getDriverResultsByRace(race))
    } catch (e: Exception) {
        Error(e)
    }

    override fun getDriverRaceResults(driverId: String): Result<DriverTotalResult> = try {
        val raceResults = remoteDataSource.getAllDriverRaceResults(driverId)
        if (raceResults.isNotEmpty()) {
            val titles = remoteDataSource.getDriverChampionships(driverId)
            Success(DriverTotalResult(titles.toString(), raceResults))
        } else {
            Error()
        }
    } catch (e: Exception) {
        Error(e)
    }

    override fun getDriverStandingsBySeason(season: String, driverId: String): Result<DriverStandingsWrapper> = try {
        Success(remoteDataSource.getDriverStandingsBySeason(season, driverId))
    } catch (e: Exception) {
        Error()
    }

    override fun getDriverSeasonStandings(season: String): Result<DriverStandingsWrapper> = try {
        Success(remoteDataSource.getDriversSeasonStandings(season))
    } catch (e: Exception) {
        Error()
    }

    override fun getDriverResultsBySeason(season: String, driverId: String): Result<List<DriverRaceResult>> = try {
        Success(remoteDataSource.getDriverResultsBySeason(season, driverId))
    } catch (e: Exception) {
        Error(e)
    }

    override fun getConstructorSeasonStandings(): Result<ConstructorStandingsWrapper> = try {
        val constructors = remoteDataSource.getConstructorSeasonStandings()
        Success(constructors)
    } catch (e: Exception) {
        Error(e)
    }

    override fun getConstructorStandingsBySeason(season: String, constructorId: String): Result<ConstructorStandingsWrapper> = try {
        val constructors = remoteDataSource.getConstructorStandingsBySeason(season, constructorId)
        Success(constructors)
    } catch (e: Exception) {
        Error(e)
    }

    override fun getAllConstructorStandingsById(constructorId: String): Result<List<ConstructorStandingsWrapper>> = try {
        val results = remoteDataSource.getAllConstructorStandingsById(constructorId)
        if (results.isNotEmpty()) {
            Success(results)
        } else {
            Error()
        }
    } catch (e: Exception) {
        Error(e)
    }
}