package com.jorgenunezvazquez.formulastats.data.repositories

import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.remote.CountriesRemoteDataSource
import com.jorgenunezvazquez.formulastats.domain.invoker.Error
import com.jorgenunezvazquez.formulastats.domain.invoker.Result
import com.jorgenunezvazquez.formulastats.domain.invoker.Success
import com.jorgenunezvazquez.formulastats.domain.repositories.CountriesRepository

class CountriesRepositoryImpl(
    private val localDataSource: CountriesLocalDataSource,
    private val remoteDataSource: CountriesRemoteDataSource
) : CountriesRepository {

    override fun loadCountries(): Result<Unit> = try {
        if (localDataSource.getCountries().isEmpty()) {
            val countries = remoteDataSource.getCountries()
            if (countries.isNotEmpty()) {
                localDataSource.saveCountries(countries)
            }
        }
        Success(Unit)
    } catch (e: Exception) {
        Error()
    }
}