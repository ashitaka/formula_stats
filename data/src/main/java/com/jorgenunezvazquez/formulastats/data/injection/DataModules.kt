package com.jorgenunezvazquez.formulastats.data.injection

import com.jorgenunezvazquez.formulastats.data.repositories.CountriesRepositoryImpl
import com.jorgenunezvazquez.formulastats.data.repositories.ImagesRepositoryImpl
import com.jorgenunezvazquez.formulastats.data.repositories.StatsRepositoryImpl
import com.jorgenunezvazquez.formulastats.domain.repositories.CountriesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.ImagesRepository
import com.jorgenunezvazquez.formulastats.domain.repositories.StatsRepository
import org.koin.dsl.module

object DataModules {

    val appModule = module {
        factory<CountriesRepository> { CountriesRepositoryImpl(get(), get()) }
        factory<StatsRepository> { StatsRepositoryImpl(get()) }
        factory<ImagesRepository> { ImagesRepositoryImpl(get(), get(), get()) }
    }
}