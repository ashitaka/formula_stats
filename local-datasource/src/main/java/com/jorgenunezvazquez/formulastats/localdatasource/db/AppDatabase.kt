package com.jorgenunezvazquez.formulastats.localdatasource.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jorgenunezvazquez.formulastats.localdatasource.CountryLocalData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@Database(entities = [CountryLocalData::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao
}

object AppDatabaseHelper {
    private const val DB_NAME = "formulaOneDatabase.db"

    fun getInstance(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DB_NAME
        ).build()
    }
}