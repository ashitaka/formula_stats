package com.jorgenunezvazquez.formulastats.localdatasource.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jorgenunezvazquez.formulastats.localdatasource.CountryLocalData

@Dao
interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCountries(countries: List<CountryLocalData>)

    @Query("SELECT * FROM countries")
    fun getCountries(): List<CountryLocalData>

    @Query("SELECT * FROM countries WHERE demonym = :countryDemonym")
    fun getCountryCodeByNationality(countryDemonym: String): CountryLocalData?

    @Query("SELECT * FROM countries WHERE name = :countryName")
    fun getCountryCodeByName(countryName: String): CountryLocalData?

}