package com.jorgenunezvazquez.formulastats.localdatasource.mappers

import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.localdatasource.CountryLocalData


object Mapper {

    fun CountryLocalData.toCountryData() = CountryData(name, alpha2Code, demonym)

    fun CountryData.toCountryLocalData() = CountryLocalData(alpha2Code, name, demonym)


}

