package com.jorgenunezvazquez.formulastats.localdatasource

import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.domain.model.countries.CountryData
import com.jorgenunezvazquez.formulastats.localdatasource.db.AppDatabase
import com.jorgenunezvazquez.formulastats.localdatasource.mappers.Mapper.toCountryData
import com.jorgenunezvazquez.formulastats.localdatasource.mappers.Mapper.toCountryLocalData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

private const val FLAG_BASE_URL = "https://flagsapi.com/%s/shiny/64.png"

class CountriesLocalDataSourceImpl : CountriesLocalDataSource, KoinComponent {

    private val appDatabase: AppDatabase by inject()

    override fun getCountryFlagUrlByNationalityString(nationality: String): String {
        /*This technical debt was added because the database has more than one dutch denonym*/
        val countryCode = if (nationality == "Dutch") {
            "NL"
        } else {
            appDatabase.countryDao().getCountryCodeByNationality(nationality)?.toCountryData()?.alpha2Code
        }
        return String.format(FLAG_BASE_URL, countryCode)
    }

    override fun getCountryFlagUrlByCountry(country: String): String {
        /*This technical debt was added because the api is not returning the official names of some countries*/
        val countryCode = when (country) {
            "UK" -> "GB"
            "USA", "United States" -> "US"
            "Russia" -> "RU"
            "UAE" -> "AE"
            else -> appDatabase.countryDao().getCountryCodeByName(country)?.alpha2Code
        }

        return String.format(FLAG_BASE_URL, countryCode)
    }

    override fun getCountries(): List<CountryData> {
        return appDatabase.countryDao().getCountries().map { it.toCountryData() }
    }

    override fun saveCountries(countries: List<CountryData>) {
        countries.let { appDatabase.countryDao().insertCountries(countries.map { it.toCountryLocalData() }) }
    }
}