package com.jorgenunezvazquez.formulastats.localdatasource.utils

import com.google.gson.Gson

object Serializer {

    fun <T> serialize(foo: T?, clazz: Class<T>): String {
        return Gson().toJson(foo, clazz)
    }

    fun <T> deserialize(string: String, clazz: Class<T>): T {
        return Gson().fromJson(string, clazz)
    }
}