package com.jorgenunezvazquez.formulastats.localdatasource.injection

import com.jorgenunezvazquez.formulastats.domain.datasources.cache.CacheDataSource
import com.jorgenunezvazquez.formulastats.domain.datasources.local.CountriesLocalDataSource
import com.jorgenunezvazquez.formulastats.localdatasource.CountriesLocalDataSourceImpl
import com.jorgenunezvazquez.formulastats.localdatasource.cache.CacheDataSourceImpl
import com.jorgenunezvazquez.formulastats.localdatasource.db.AppDatabase
import com.jorgenunezvazquez.formulastats.localdatasource.db.AppDatabaseHelper
import org.koin.dsl.module

object LocalModules {

    val localModules = module {

        single { AppDatabaseHelper.getInstance(get()) }
        factory<CountriesLocalDataSource> { CountriesLocalDataSourceImpl() }
        factory<CacheDataSource> { CacheDataSourceImpl() }
    }
}