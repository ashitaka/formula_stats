package com.jorgenunezvazquez.formulastats.localdatasource.cache

import android.content.Context
import com.jorgenunezvazquez.formulastats.domain.datasources.cache.CacheDataSource
import com.jorgenunezvazquez.formulastats.localdatasource.utils.FileManager
import com.jorgenunezvazquez.formulastats.localdatasource.utils.Serializer
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import java.io.File

class CacheDataSourceImpl : CacheDataSource, KoinComponent {

    override fun isImageUrlCached(wikiTitle: String):Boolean {
        val driverImageUrlFile: File = buildDriverImageUrlFile(wikiTitle)
        return FileManager.exists(driverImageUrlFile)
    }

    override fun getImageUrl(wikiTitle: String): String {
        val driverImageUrlFile: File = buildDriverImageUrlFile(wikiTitle)
        return Serializer.deserialize(FileManager.readFileContent(driverImageUrlFile), String::class.java)
    }

    override fun putImageUrl(wikiTitle: String, driverImageUrl: String) {
        val driverStandingFile: File = buildDriverImageUrlFile(wikiTitle)
        val jsonString: String = Serializer.serialize(driverImageUrl, String::class.java)
        FileManager.writeToFile(driverStandingFile, jsonString)
    }

    private fun buildDriverImageUrlFile(wikiTitle: String): File {
        val fileName: String = wikiTitle
        return File(get<Context>().cacheDir, fileName)
    }
}