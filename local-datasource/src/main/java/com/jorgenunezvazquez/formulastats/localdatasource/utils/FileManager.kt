package com.jorgenunezvazquez.formulastats.localdatasource.utils

import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.FileWriter

object FileManager {

    fun writeToFile(file: File, fileContent: String) {
        if (!file.exists()) {
            FileWriter(file).apply {
                write(fileContent)
                close()
            }
        }
    }

    fun readFileContent(file: File): String {
        var content = ""
        if (file.exists()) {
            content = BufferedReader(FileReader(file)).use(BufferedReader::readText)
        }
        return content
    }

    fun exists(file: File): Boolean {
        return file.exists()
    }
}