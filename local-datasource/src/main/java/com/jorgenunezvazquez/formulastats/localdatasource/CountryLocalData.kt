package com.jorgenunezvazquez.formulastats.localdatasource

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "countries")
data class CountryLocalData(
    @PrimaryKey val alpha2Code: String,
    val name: String,
    val demonym: String
)